// English

/* Class */ "Class",
/* Concept */ "Concept",
/* Define */ "Define",
/* Directory */ "Directory",
/* Enum */ "Enum",
/* File */ "File",
/* Function */ "Function",
/* Group */ "Group",
/* Namespace */ "Namespace",
/* Page */ "Page",
/* Struct */ "Struct",
/* Typedef */ "Typedef",
/* Union */ "Union",
/* Variable */ "Variable",

/* DirectoryPath */ "Directory path",
/* ParentDirectory */ "Parent directory",
/* Subdirectories */ "Subdirectories",
/* Files */ "Files"
