#include "inhale/l10n/ids.hpp"
#include "inhale/l10n/language.hpp"

#include <magic_enum.hpp>

namespace Inhale {
namespace L10n {
namespace {

template<typename _EnumT>
constexpr std::size_t
c() noexcept
{
    return magic_enum::enum_count<_EnumT>();
}

template<typename _EnumT>
constexpr std::size_t
i(_EnumT _e) noexcept
{
    return static_cast<std::size_t>(_e);
}

std::string_view const s_translations[c<Language>()][c<Id>()] = {
    {
        #include "translations/am.inc"
    },
    {
        #include "translations/ar.inc"
    },
    {
        #include "translations/bg.inc"
    },
    {
        #include "translations/br.inc"
    },
    {
        #include "translations/ca.inc"
    },
    {
        #include "translations/cn.inc"
    },
    {
        #include "translations/cz.inc"
    },
    {
        #include "translations/de.inc"
    },
    {
        #include "translations/dk.inc"
    },
    {
        #include "translations/en.inc"
    },
    {
        #include "translations/eo.inc"
    },
    {
        #include "translations/es.inc"
    },
    {
        #include "translations/fa.inc"
    },
    {
        #include "translations/fi.inc"
    },
    {
        #include "translations/fr.inc"
    },
    {
        #include "translations/gr.inc"
    },
    {
        #include "translations/hi.inc"
    },
    {
        #include "translations/hr.inc"
    },
    {
        #include "translations/hu.inc"
    },
    {
        #include "translations/id.inc"
    },
    {
        #include "translations/it.inc"
    },
    {
        #include "translations/je.inc"
    },
    {
        #include "translations/jp.inc"
    },
    {
        #include "translations/ke.inc"
    },
    {
        #include "translations/kr.inc"
    },
    {
        #include "translations/lt.inc"
    },
    {
        #include "translations/lv.inc"
    },
    {
        #include "translations/mk.inc"
    },
    {
        #include "translations/nl.inc"
    },
    {
        #include "translations/no.inc"
    },
    {
        #include "translations/pl.inc"
    },
    {
        #include "translations/pt.inc"
    },
    {
        #include "translations/ro.inc"
    },
    {
        #include "translations/ru.inc"
    },
    {
        #include "translations/sc.inc"
    },
    {
        #include "translations/si.inc"
    },
    {
        #include "translations/sk.inc"
    },
    {
        #include "translations/sr.inc"
    },
    {
        #include "translations/sv.inc"
    },
    {
        #include "translations/tr.inc"
    },
    {
        #include "translations/tw.inc"
    },
    {
        #include "translations/ua.inc"
    },
    {
        #include "translations/vi.inc"
    },
    {
        #include "translations/za.inc"
    }
};

Language s_language = Language::en;

} // namespace

void
setLanguage(Language _lang) noexcept
{
    s_language = _lang;
}

std::string_view
getString(Id _id) noexcept
{
    const auto result = s_translations[i(s_language)][i(_id)];
    if (result.empty())
    {
        return s_translations[i(Language::en)][i(_id)];
    }

    return result;
}

} // namespace L10n
} // namespace Inhale
