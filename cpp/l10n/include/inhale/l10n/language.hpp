#ifndef __INHALE_L10N_LANGUAGE_HPP__
#define __INHALE_L10N_LANGUAGE_HPP__

#include <cstdint>

namespace Inhale {
namespace L10n {

enum class Language: std::uint8_t
{
   am,
   ar,
   bg,
   br,
   ca,
   cn,
   cz,
   de,
   dk,
   en,
   eo,
   es,
   fa,
   fi,
   fr,
   gr,
   hi,
   hr,
   hu,
   id,
   it,
   je,
   jp,
   ke,
   kr,
   lt,
   lv,
   mk,
   nl,
   no,
   pl,
   pt,
   ro,
   ru,
   sc,
   si,
   sk,
   sr,
   sv,
   tr,
   tw,
   ua,
   vi,
   za,
};

void
setLanguage(Language _lang) noexcept;

} // namespace L10n
} // namespace Inhale

#endif // __INHALE_L10N_LANGUAGE_HPP__
