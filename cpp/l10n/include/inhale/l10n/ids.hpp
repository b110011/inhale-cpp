#ifndef __INHALE_L10N_IDS_HPP__
#define __INHALE_L10N_IDS_HPP__

#include <cstdint>
#include <string_view>

namespace Inhale {
namespace L10n {

enum class Id: std::uint8_t
{
    // Compound types
    Class = 0,
    Concept,
    Define,
    Directory,
    Enum,
    File,
    Function,
    Group,
    Namespace,
    Page,
    Struct,
    Typedef,
    Union,
    Variable,
    // Directories
    DirectoryPath,
    ParentDirectory,
    Subdirectories,
    Files,
};

std::string_view
getString(Id _id) noexcept;

} // namespace L10n
} // namespace Inhale

#define _(_ID) ::Inhale::L10n::getString(::Inhale::L10n::Id::_ID)

#endif // __INHALE_L10N_IDS_HPP__
