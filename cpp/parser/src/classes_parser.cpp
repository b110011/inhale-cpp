#include "src/classes_parser.hpp"

#include "inhale/storage/compounds/data_structure.hpp"
#include "inhale/storage/context.hpp"
#include "inhale/storage/inheritances.hpp"
#include "inhale/storage/relations.hpp"
#include "inhale/logging.hpp"

#include "src/graph_utils.hpp"
#include "src/xml_parser.hpp"

#include <magic_enum.hpp>

namespace Inhale {
namespace Parser {
namespace {

[[nodiscard]]
constexpr std::size_t
hash(std::string_view _prot, std::string_view _virt) noexcept
{
    if (_prot.size() < 3 || _virt.empty())
    {
        return 0;
    }

    // https://codereview.stackexchange.com/questions/83170
    return static_cast<std::size_t>(
        _prot[0] | (_prot[1] << 8) | (_prot[2] << 16) | (_virt[0] << 24)
    );
}

[[nodiscard]]
Storage::InheritanceType
toInheritanceType(pugi::xml_node _xmlNode)
{
    auto const prot = _xmlNode.attribute("prot").value();
    auto const virt = _xmlNode.attribute("virt").value();

    static_assert(magic_enum::enum_count<Storage::InheritanceType>() == 6);
    switch (hash(prot, virt))
    {
        case hash("private", "non-virtual"):
            return Storage::InheritanceType::Private;

        case hash("private", "virtual"):
            return Storage::InheritanceType::VirtualPrivate;

        case hash("protected", "non-virtual"):
            return Storage::InheritanceType::Protected;

        case hash("protected", "virtual"):
            return Storage::InheritanceType::VirtualProtected;

        case hash("public", "non-virtual"):
            return Storage::InheritanceType::Public;

        case hash("public", "virtual"):
            return Storage::InheritanceType::VirtualPublic;

        default:
            INHALE_ERROR("Could not parse '{}' prot and '{}' virt combination!", prot, virt);
            INHALE_DEBUG("Use `grep 'prot=\"{}\" virt=\"{}\"' -r .` to find more.", prot, virt);
            throw std::runtime_error{"Could not parse prot and virt combination!"};
    }
}

} // namespace

void
parseClass(Storage::Context &_ctx, Storage::DataStructure &_ds, pugi::xml_node _doc)
{
    auto const cdef = _doc.child("doxygen").child("compounddef");

    for (auto const param : cdef.child("templateparamlist").children("param"))
    {
        /* Doxygen seems to produce unreliable results.

           For example, sometimes you will get `param.type <- class X` with empty
           `declname` and `defname`, and sometimes you will get `param.type <- class`
           and `decl` name `X`. Similar behavior is observed with `typename X`.

           These are generally just ignored (falling in the broader category of a
           typename).

           Sometimes you will get a refid in the type, so deal with that as they
           come too (yay)!
         */

        std::string_view type{param.child_value("type")};
        std::string_view declname{param.child_value("declname")};
        std::string_view defname{param.child_value("defname")};

        // TODO: this doesn't seem to happen, should probably investigate more
        // do something with `param.defval` ?

        /* When decl_n and def_n are the same, this means no explicit default
           template parameter is given. This will ultimately mean that def_n is
           set to None for consistency.
         */
        if (declname == defname)
        {
            defname = {};
        }

        std::string_view refid{param.child("type").attribute("refid").value()};
        if (refid.empty())
        {
            _ds.addTemplateParam({
                std::string{type},
                std::string{declname},
                std::string{defname}
            });
            continue;
        }

        _ds.addTemplateParam({
            _ctx.getCompoundById(refid),
            std::string{declname},
            std::string{defname}
        });
    }

    auto &inheritances = _ctx.takeInheritances();
    for (auto const bcf : cdef.children("basecompoundref"))
    {
        auto const refid = bcf.attribute("refid").value();
        auto const &node = _ctx.getCompoundById(refid);
        auto const itype = toInheritanceType(bcf);

        inheritances.add(node, _ds, itype);
    }

    for (auto const dcf : cdef.children("derivedcompoundref"))
    {
        auto const refid = dcf.attribute("refid").value();
        auto const &node = _ctx.getCompoundById(refid);
        auto const itype = toInheritanceType(dcf);

        inheritances.add(_ds, node, itype);
    }

    auto &relations = _ctx.takeRelations();
    for (auto const ic : cdef.children("innerclass"))
    {
        auto const refid = ic.attribute("refid").value();
        auto const &node = _ctx.getCompoundById(refid);

        relations.add(_ds, node);
    }
}

void
parseClasses(Storage::Context &_ctx, Config const &_config)
{
    auto callback = [&_ctx, &_config](Storage::Compound &_compound)
    {
        auto const doc = parseXmlFile(getFilePath(_config, _compound));
        parseClass(_ctx, static_cast<Storage::DataStructure &>(_compound), doc);

        return true;
    };

    _ctx.forEachCompoundByType(Storage::CompoundType::Class, callback);
    _ctx.forEachCompoundByType(Storage::CompoundType::Struct, callback);
}

} // namespace Parser
} // namespace Inhale
