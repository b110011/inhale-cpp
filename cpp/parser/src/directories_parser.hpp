#ifndef __INHALE_PARSER_SRC_DIRECTORIESDOXYGEN_HPP__
#define __INHALE_PARSER_SRC_DIRECTORIESDOXYGEN_HPP__

#include "inhale/parser_fwd.hpp"
#include "inhale/storage_fwd.hpp"

#include <pugixml.hpp>

namespace Inhale {
namespace Parser {

void
parseDirectory(
    Storage::Context &_ctx,
    Storage::Compound const &_dir,
    pugi::xml_node _doc
);

void
parseDirectories(Storage::Context &_ctx, Config const &_config);

} // namespace Parser
} // namespace Inhale

#endif // __INHALE_PARSER_SRC_DIRECTORIESDOXYGEN_HPP__
