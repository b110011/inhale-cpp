#ifndef __INHALE_PARSER_SRC_PAGESDOXYGEN_HPP__
#define __INHALE_PARSER_SRC_PAGESDOXYGEN_HPP__

#include "inhale/parser_fwd.hpp"
#include "inhale/storage_fwd.hpp"

#include <pugixml.hpp>

namespace Inhale {
namespace Parser {

void
parsePage(
    Storage::Context &_ctx,
    Config const &_config,
    Storage::Page &_page,
    pugi::xml_node _doc
);

void
parsePages(Storage::Context &_ctx, Config const &_config);

} // namespace Parser
} // namespace Inhale

#endif // __INHALE_PARSER_SRC_PAGESDOXYGEN_HPP__
