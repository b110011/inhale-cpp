#include "src/namespaces_parser.hpp"

#include "inhale/storage/compound.hpp"
#include "inhale/storage/context.hpp"
#include "inhale/storage/relations.hpp"
#include "inhale/logging.hpp"

#include "src/graph_utils.hpp"
#include "src/section_parser.hpp"
#include "src/xml_parser.hpp"

namespace Inhale {
namespace Parser {
namespace {

void
ensureSingleDefinition(
    Storage::Context const &_ctx,
    Storage::Compound const &_page,
    Storage::Compound const &_child
)
{
    _ctx.getRelations().forEachParentByType(
        _child,
        Storage::CompoundType::Namespace,
        [&_child, &_page](Storage::Compound const &_parent) -> bool
        {
            INHALE_ERROR(
                "'{}' appears to be defined in both '{}' and '{}'!",
                _child.getName(),
                _parent.getName(),
                _page.getName()
            );

            throw std::runtime_error{"Compound appears to be defined at least twice!"};
        }
    );
}

} // namespace

void
parseNamespace(
    Storage::Context &_ctx,
    Config const &_config,
    Storage::Compound &_ns,
    pugi::xml_node _doc
)
{
    auto const cdef = _doc.child("doxygen").child("compounddef");

    // Seems like namespaces in 'index.xml' don't list 'innerclass' and
    // 'innernamespace' in their 'members' but list 'function' and 'variable'.
    // We need to bind missing relations here.

    for (auto const ic : cdef.children("innerclass"))
    {
        auto const refid = ic.attribute("refid").value();
        auto const &node = _ctx.getCompoundById(refid);

        ensureSingleDefinition(_ctx, _ns, node);
        _ctx.takeRelations().add(_ns, node);
    }

    for (auto const ins : cdef.children("innernamespace"))
    {
        auto const refid = ins.attribute("refid").value();
        auto const &node = _ctx.getCompoundById(refid);

        ensureSingleDefinition(_ctx, _ns, node);
        _ctx.takeRelations().add(_ns, node);
    }

    NamespaceSectionParser{_ctx, _ns, _config}.parse(cdef);
}

void
parseNamespaces(Storage::Context &_ctx, Config const &_config)
{
    _ctx.forEachCompoundByType(
        Storage::CompoundType::Namespace,
        [&_ctx, &_config](Storage::Compound &_compound)
        {
            auto const doc = parseXmlFile(getFilePath(_config, _compound));
            parseNamespace(_ctx, _config, _compound, doc);

            return true;
        }
    );
}

} // namespace Parser
} // namespace Inhale
