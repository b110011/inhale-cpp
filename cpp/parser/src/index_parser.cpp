#include "src/index_parser.hpp"

#include "inhale/parser_config.hpp"
#include "inhale/storage/compound_type.hpp"
#include "inhale/storage/compound.hpp"
#include "inhale/storage/relations.hpp"
#include "inhale/storage/context.hpp"

#include "src/graph_utils.hpp"
#include "src/xml_parser.hpp"

#include <cassert>

namespace Inhale {
namespace Parser {
namespace {

[[nodiscard]]
Storage::Compound &
addCompound(Storage::Context &_ctx, pugi::xml_node _xmlNode)
{
    auto const refid = _xmlNode.attribute("refid").value();
    auto const type = toCompoundType(_xmlNode.attribute("kind").value());
    auto const name = _xmlNode.child_value("name");

    assert(refid /*&& type*/ && name);
    return _ctx.createCompound(refid, name, type);
}

} // namespace

void
parseIndex(Storage::Context &_ctx, pugi::xml_node _doc)
{
    for (auto const compound : _doc.child("doxygenindex").children("compound"))
    {
        auto &c = addCompound(_ctx, compound);

        /* For things like files and namespaces, a "member" list will include
           things like defines, enums, etc.

           For other type we don't need to pay attention because "breathe" will
           help all that.
         */
        using Type = Storage::CompoundType;
        if ((c.getType() == Type::File) || (c.getType() == Type::Namespace))
        {
            for (auto const member : compound.children("member"))
            {
                _ctx.takeRelations().add(c, addCompound(_ctx, member));
            }
        }
    }
}

void
parseIndex(Storage::Context &_ctx, Config const &_config)
{
    parseIndex(_ctx, parseXmlFile(_config.doxygenXmlOutputDir / "index.xml"));
}

} // namespace Parser
} // namespace Inhale
