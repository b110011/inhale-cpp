#ifndef __INHALE_PARSER_SRC_SECTIONDOXYGEN_HPP__
#define __INHALE_PARSER_SRC_SECTIONDOXYGEN_HPP__

#include "inhale/parser_fwd.hpp"
#include "inhale/storage_fwd.hpp"

#include <pugixml.hpp>

#include <functional>
#include <string_view>

namespace Inhale {
namespace Parser {

class BaseSectionParser
{
public:

    BaseSectionParser(
        Storage::Context &_ctx,
        Storage::Compound const &_parent,
        Config const &_config
    ) noexcept;

    virtual ~BaseSectionParser() = default;


    void parse(pugi::xml_node _xmlNode);

private:

    void parseFunction(pugi::xml_node _xmlNode);

    [[nodiscard]]
    virtual Storage::Compound &takeCompoundById(pugi::xml_node _xmlNode) = 0;

protected:

    Storage::Context &m_ctx;
    Storage::Compound const &m_parent;
    Config const &m_config;
};

class FileSectionParser final
    : public BaseSectionParser
{
public:

    using BaseSectionParser::BaseSectionParser;

private:

    Storage::Compound &takeCompoundById(pugi::xml_node _xmlNode) final;
};

class NamespaceSectionParser final
    : public BaseSectionParser
{
public:

    using BaseSectionParser::BaseSectionParser;

private:

    Storage::Compound &takeCompoundById(pugi::xml_node _xmlNode) final;
};

} // namespace Parser
} // namespace Inhale

#endif // __INHALE_PARSER_SRC_SECTIONDOXYGEN_HPP__
