#ifndef __INHALE_PARSER_SRC_INDEXDOXYGEN_HPP__
#define __INHALE_PARSER_SRC_INDEXDOXYGEN_HPP__

#include "inhale/parser_fwd.hpp"
#include "inhale/storage_fwd.hpp"

#include <pugixml.hpp>

namespace Inhale {
namespace Parser {

void parseIndex(Storage::Context &_ctx, pugi::xml_node _doc);

void parseIndex(Storage::Context &_ctx, Config const &_config);

} // namespace Parser
} // namespace Inhale

#endif // __INHALE_PARSER_SRC_INDEXDOXYGEN_HPP__
