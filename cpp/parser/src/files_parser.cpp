#include "src/files_parser.hpp"

#include "inhale/storage/compounds/file.hpp"
#include "inhale/storage/context.hpp"
#include "inhale/storage/relations.hpp"
#include "inhale/logging.hpp"

#include "src/graph_utils.hpp"
#include "src/section_parser.hpp"
#include "src/xml_parser.hpp"

#include <magic_enum.hpp>

namespace Inhale {
namespace Parser {
namespace {

[[nodiscard]]
constexpr std::size_t
hash(std::string_view _str) noexcept
{
    if (_str.empty())
    {
        return 0;
    }

    if (_str.size() == 1)
    {
        return static_cast<std::size_t>(_str.back());
    }

    return static_cast<std::size_t>(_str.front() | (_str.back() << 8));
}

[[nodiscard]]
Storage::FileLanguage
toFileLanguage(std::string_view _str)
{
    // ATTENTION(b110011): Keep in sync with 'doxygen/src/trace.h' (fmt::formatter<SrcLangExt>).

    static_assert(magic_enum::enum_count<Storage::FileLanguage>() == 20);
    switch (hash(_str))
    {
        case hash("C"):
            return Storage::FileLanguage::C;

        case hash("C#"):
            return Storage::FileLanguage::CSharp;

        case hash("C++"):
            return Storage::FileLanguage::Cpp;

        case hash("D"):
            return Storage::FileLanguage::D;

        case hash("Fortran"):
            return Storage::FileLanguage::Fortran;

        case hash("FortranFixed"):
            return Storage::FileLanguage::FortranFixed;

        case hash("FortranFree"):
            return Storage::FileLanguage::FortranFree;

        case hash("IDL"):
            return Storage::FileLanguage::Idl;

        case hash("Java"):
            return Storage::FileLanguage::Java;

        case hash("Javascript"):
            return Storage::FileLanguage::Javascript;

        case hash("Lex"):
            return Storage::FileLanguage::Lex;

        case hash("Markdown"):
            return Storage::FileLanguage::Markdown;

        case hash("Objecive-C"):
            return Storage::FileLanguage::ObjectiveC;

        case hash("PHP"):
            return Storage::FileLanguage::Php;

        case hash("Python"):
            return Storage::FileLanguage::Python;

        case hash("Slice"):
            return Storage::FileLanguage::Slice;

        case hash("SQL"):
            return Storage::FileLanguage::Sql;

        case hash("Verilog"):
            return Storage::FileLanguage::Verilog;

        case hash("VHDL"):
            return Storage::FileLanguage::Vhdl;

        case hash("XML"):
            return Storage::FileLanguage::Xml;

        default:
            INHALE_ERROR("Could not parse '{}' language!", _str);
            throw std::runtime_error{"Could not parse language!"};
    }
}

void
ensureSingleDefinition(
    Storage::Context &_ctx,
    Storage::Compound const &_file,
    Storage::Compound const &_child
)
{
    _ctx.getRelations().forEachParentByType(
        _file,
        Storage::CompoundType::File,
        [&_child, &_file](Storage::Compound const &_parent)
        {
            if (&_parent == &_file)
            {
                return true;
            }

            INHALE_ERROR(
                "'{}' appears to be defined in both '{}' and '{}'!",
                _child.getName(),
                _parent.getName(),
                _file.getName()
            );

            throw std::runtime_error{"Compound appears to be defined at least twice!"};
        }
    );
}

} // namespace

void
parseFile(
    Storage::Context &_ctx,
    Config const &_config,
    Storage::File &_file,
    pugi::xml_node _doc
)
{
    auto const cdef = _doc.child("doxygen").child("compounddef");

    _file.setLanguage(toFileLanguage(cdef.attribute("language").value()));
    _file.setSourceLocation(std::string{getSourceLocation(_config, cdef)});

    // _ctx.reloc(_file);

    /* TODO
    for (auto const include : cdef.children("includes"))
    {

    }

    for (auto const includer : cdef.children("includedby"))
    {

    }
    */

    for (auto const ic : cdef.children("innerclass"))
    {
        auto const refid = ic.attribute("refid").value();
        auto &node = _ctx.takeCompoundById(refid);

        ensureSingleDefinition(_ctx, _file, node);
        _ctx.takeRelations().add(_file, node);
    }

    // Skip innernamespace.

    for (auto const mdef : cdef.children("memberdef"))
    {
        auto const id = mdef.attribute("id").value();
        auto &node = _ctx.takeCompoundById(id);

        ensureSingleDefinition(_ctx, _file, node);
        _ctx.takeRelations().add(_file, node);
    }

    FileSectionParser{_ctx, _file, _config}.parse(cdef);
}

void
parseFiles(Storage::Context &_ctx, Config const &_config)
{
    _ctx.forEachCompoundByType(
        Storage::CompoundType::File,
        [&_ctx, &_config](Storage::Compound &_compound)
        {
            auto const doc = parseXmlFile(getFilePath(_config, _compound));
            parseFile(_ctx, _config, static_cast<Storage::File &>(_compound), doc);

            return true;
        }
    );
}

} // namespace Parser
} // namespace Inhale
