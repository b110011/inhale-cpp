#include "src/directories_parser.hpp"

#include "inhale/storage/context.hpp"
#include "inhale/storage/relations.hpp"
#include "inhale/logging.hpp"

#include "src/graph_utils.hpp"
#include "src/xml_parser.hpp"

namespace Inhale {
namespace Parser {
namespace {

void
ensureSingleDefinition(
    Storage::Context const &_ctx,
    Storage::Compound const &_newParent,
    Storage::Compound const &_dir
)
{
    _ctx.getRelations().forEachParentByType(
        _dir,
        Storage::CompoundType::Directory,
        [&_newParent, &_dir](Storage::Compound const &_parent) -> bool
        {
            INHALE_ERROR(
                "'{}' appears to be defined in both '{}' and '{}'!",
                _dir.getName(),
                _parent.getName(),
                _newParent.getName()
            );

            throw std::runtime_error{"Compound appears to be defined at least twice!"};
        }
    );
}

} // namespace

void
parseDirectory(
    Storage::Context &_ctx,
    Storage::Compound const &_dir,
    pugi::xml_node _doc
)
{
    auto const cdef = _doc.child("doxygen").child("compounddef");

    for (auto const idir : cdef.children("innerdir"))
    {
        auto const refid = idir.attribute("refid").value();
        auto const &node = _ctx.getCompoundById(refid);

        ensureSingleDefinition(_ctx, _dir, node);
        _ctx.takeRelations().add(_dir, node);
    }

    for (auto const ifile : cdef.children("innerfile"))
    {
        auto const refid = ifile.attribute("refid").value();
        auto const &node = _ctx.getCompoundById(refid);

        ensureSingleDefinition(_ctx, _dir, node);
        _ctx.takeRelations().add(_dir, node);
    }
}

void
parseDirectories(Storage::Context &_ctx, Config const &_config)
{
    _ctx.forEachCompoundByType(
        Storage::CompoundType::Directory,
        [&_ctx, &_config](Storage::Compound const &_dir)
        {
            auto const doc = parseXmlFile(getFilePath(_config, _dir));
            parseDirectory(_ctx, _dir, doc);

            return true;
        }
    );
}

} // namespace Parser
} // namespace Inhale
