#ifndef __INHALE_PARSER_SRC_XMLDOXYGEN_HPP__
#define __INHALE_PARSER_SRC_XMLDOXYGEN_HPP__

#include "inhale/common/format/filesystem.hpp"
#include "inhale/parser_config.hpp"
#include "inhale/logging.hpp"

#include <pugixml.hpp>

#include <filesystem>
#include <string_view>
#include <string>

namespace Inhale {
namespace Parser {

[[nodiscard]]
inline std::string_view
getSourceLocation(Config const &/*_config*/, pugi::xml_node _xmlNode)
{
    std::string_view loc{_xmlNode.child("location").attribute("file").value()};

    // Some older versions of doxygen don't reliably strip from path, so make sure
    // to remove it.
    // if (loc.starts_with(_config.sourcesDir))
    // {
    //     return loc.substr(_config.sourcesDir.size());
    // }

    return loc;
}

[[nodiscard]]
inline pugi::xml_document
parseXmlFile(std::filesystem::path const &_path)
{
    if (!std::filesystem::exists(_path))
    {
        INHALE_ERROR("Could not find '{}' XML file!", _path);
        throw std::runtime_error{"Could not find XML file!"};
    }

    pugi::xml_document doc;
    if (!doc.load_file(_path.c_str()))
    {
        INHALE_ERROR("Could not to parse '{}' XML file!", _path);
        throw std::runtime_error{"Could not to parse XML file!"};
    }

    return doc;
}

[[nodiscard]]
inline std::string
sanitize(std::string _data)
{
    auto replace = [](std::string &_s, std::string_view _old, std::string_view _new)
    {
        _s.replace(_s.find(_old), _old.size(), _new);
    };

    replace(_data, "&lt;", "<");
    replace(_data, "&gt;", ">");
    replace(_data, "&amp;", "&");
    replace(_data, "< ", "<");
    replace(_data, " >", ">");
    replace(_data, " &", "&");
    replace(_data, "& ", "&");

    return _data;
}

} // namespace Parser
} // namespace Inhale

#endif // __INHALE_PARSER_SRC_XMLDOXYGEN_HPP__
