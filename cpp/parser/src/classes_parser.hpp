#ifndef __INHALE_PARSER_SRC_CLASSESDOXYGEN_HPP__
#define __INHALE_PARSER_SRC_CLASSESDOXYGEN_HPP__

#include "inhale/parser_fwd.hpp"
#include "inhale/storage_fwd.hpp"

#include <pugixml.hpp>

namespace Inhale {
namespace Parser {

void
parseClass(Storage::Context &_ctx, Storage::DataStructure &_ds, pugi::xml_node _doc);

void
parseClasses(Storage::Context &_ctx, Config const &_config);

} // namespace Parser
} // namespace Inhale

#endif // __INHALE_PARSER_SRC_CLASSESDOXYGEN_HPP__
