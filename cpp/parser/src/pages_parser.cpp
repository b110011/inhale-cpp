#include "src/pages_parser.hpp"

#include "inhale/storage/compounds/page.hpp"
#include "inhale/storage/context.hpp"
#include "inhale/storage/relations.hpp"
#include "inhale/logging.hpp"

#include "src/graph_utils.hpp"
#include "src/xml_parser.hpp"

namespace Inhale {
namespace Parser {
namespace {

void
ensureSingleDefinition(
    Storage::Context const &_ctx,
    Storage::Compound const &_page,
    Storage::Compound const &_child
)
{
    _ctx.getRelations().forEachParentByType(
        _child,
        Storage::CompoundType::Page,
        [&_child, &_page](Storage::Compound const &_parent) -> bool
        {
            INHALE_ERROR(
                "'{}' appears to be defined in both '{}' and '{}'!",
                _child.getName(),
                _parent.getName(),
                _page.getName()
            );

            throw std::runtime_error{"Compound appears to be defined at least twice!"};
        }
    );
}

} // namespace

void
parsePage(
    Storage::Context &_ctx,
    Config const &_config,
    Storage::Page &_page,
    pugi::xml_node _doc
)
{
    auto const cdef = _doc.child("doxygen").child("compounddef");

    _page.setSourceLocation(std::string{getSourceLocation(_config, cdef)});
    _page.setTitle(cdef.child_value("title"));

    // _ctx.reloc(_page);

    for (auto const ip : cdef.children("innerpage"))
    {
        auto const refid = ip.attribute("refid").value();
        auto const &node = _ctx.getCompoundById(refid);

        ensureSingleDefinition(_ctx, _page, node);
        _ctx.takeRelations().add(_page, node);
    }

    // TODO: self.pages = [page for page in self.pages if not page.parent]
}

void
parsePages(Storage::Context &_ctx, Config const &_config)
{
    _ctx.forEachCompoundByType(
        Storage::CompoundType::Page,
        [&_ctx, &_config](Storage::Compound &_compound)
        {
            auto const doc = parseXmlFile(getFilePath(_config, _compound));
            parsePage(_ctx, _config, static_cast<Storage::Page &>(_compound), doc);

            return true;
        }
    );
}

} // namespace Parser
} // namespace Inhale
