#ifndef __INHALE_PARSER_SRC_GRAPHUTILS_HPP__
#define __INHALE_PARSER_SRC_GRAPHUTILS_HPP__

#include "inhale/parser_config.hpp"
#include "inhale/storage/compound.hpp"
#include "inhale/storage/context.hpp"

namespace Inhale {
namespace Parser {

[[nodiscard]]
inline std::filesystem::path
getFilePath(Config const &_config, Storage::Compound const &_compound)
{
    auto nodeXmlFilePath = _config.doxygenXmlOutputDir / _compound.getId();
    nodeXmlFilePath.replace_extension("xml");

    return nodeXmlFilePath;
}

[[nodiscard]]
Storage::CompoundType
toCompoundType(std::string_view _str);

} // namespace Parser
} // namespace Inhale

#endif // __INHALE_PARSER_SRC_GRAPHUTILS_HPP__
