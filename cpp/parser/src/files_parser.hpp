#ifndef __INHALE_PARSER_SRC_FILESDOXYGEN_HPP__
#define __INHALE_PARSER_SRC_FILESDOXYGEN_HPP__

#include "inhale/parser_fwd.hpp"
#include "inhale/storage_fwd.hpp"

#include <pugixml.hpp>

namespace Inhale {
namespace Parser {

void
parseFile(
    Storage::Context &_ctx,
    Config const &_config,
    Storage::File &_file,
    pugi::xml_node _doc
);

void
parseFiles(Storage::Context &_ctx, Config const &_config);

} // namespace Parser
} // namespace Inhale

#endif // __INHALE_PARSER_SRC_FILESDOXYGEN_HPP__
