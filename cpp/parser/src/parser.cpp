#include "inhale/parser.hpp"

#include "src/classes_parser.hpp"
#include "src/directories_parser.hpp"
#include "src/files_parser.hpp"
#include "src/index_parser.hpp"
#include "src/namespaces_parser.hpp"
#include "src/pages_parser.hpp"

namespace Inhale {
namespace Parser {

void
parse(Storage::Context &_ctx, Config const &_config)
{
    parseIndex(_ctx, _config);
    parsePages(_ctx, _config);
    parseFiles(_ctx, _config);
    parseNamespaces(_ctx, _config);
    parseClasses(_ctx, _config);
    parseDirectories(_ctx, _config);
}

} // namespace Parser
} // namespace Inhale
