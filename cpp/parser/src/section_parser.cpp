#include "src/section_parser.hpp"

#include "inhale/storage/compounds/function.hpp"
#include "inhale/storage/context.hpp"
#include "inhale/storage/relations.hpp"

#include "src/graph_utils.hpp"
#include "src/xml_parser.hpp"

#include <cassert>
#include <cstring>

namespace Inhale {
namespace Parser {

BaseSectionParser::BaseSectionParser(
    Storage::Context &_ctx,
    Storage::Compound const &_parent,
    Config const &_config
) noexcept
    : m_ctx{_ctx}
    , m_parent{_parent}
    , m_config{_config}
{}

void
BaseSectionParser::parse(pugi::xml_node _xmlNode)
{
    for (auto const sdef : _xmlNode.children("sectiondef"))
    {
        if (std::strcmp(sdef.attribute("kind").value(), "func") == 0)
        {
             parseFunction(sdef);
             continue;
        }

        for (auto const mdef : _xmlNode.children("memberdef"))
        {
            m_ctx.takeRelations().add(m_parent, takeCompoundById(mdef));
        }
    }
}

void
BaseSectionParser::parseFunction(pugi::xml_node _xmlNode)
{
    for (auto const mdef : _xmlNode.children("memberdef"))
    {
        assert(std::strcmp(mdef.attribute("kind").value(), "function") == 0);
        auto &func = static_cast<Storage::Function &>(takeCompoundById(mdef));

        func.setReturnType(sanitize(mdef.child_value("type")));

        for (auto const param : mdef.children("param"))
        {
            func.addParam(sanitize(param.child_value("type")));
        }

        if (auto const tpl = mdef.child("templateparamlist"))
        {
            for (auto const param : tpl.children("param"))
            {
                func.addTemplateParam(sanitize(param.child_value("type")));
            }
        }

        m_ctx.takeRelations().add(m_parent, func);
    }
}

Storage::Compound &
FileSectionParser::takeCompoundById(pugi::xml_node _xmlNode)
{
    auto const id{_xmlNode.attribute("id").value()};
    return m_ctx.takeCompoundById(id);
}

/* There is a weird behavior which appears at least on 1.9.4 version of doxygen.

Below code doesn't define 'add' method at all. You don't have it neither in
'sectiondef.memberdef' nor in 'programlisting'. I guess that's why doxygen also
doesn't add that method to file 'members' in 'index.xml'.

But it appears in 'sectiondef.memberdef' of the parent namespace and even has a
correct location.

    #ifndef __EXAMPLE_HPP__
    #define __EXAMPLE_HPP__

    namespace Example {

    template<typename T>
    T add(T a, T b)
    {
        return a + b;
    }

    } // namespace Example

    #endif // __EXAMPLE_HPP__

 */

Storage::Compound &
NamespaceSectionParser::takeCompoundById(pugi::xml_node _xmlNode)
{
    std::string_view id{_xmlNode.attribute("id").value()};

    try
    {
        return m_ctx.takeCompoundById(id);
    }
    catch(std::out_of_range const &)
    {
        // NOTE(b110011): Do nothing, it's expected.
    }

    auto const type = toCompoundType(_xmlNode.attribute("kind").value());
    auto const name = _xmlNode.child_value("name");

    assert(!id.empty() /*&& type*/ && name);
    auto &compound = m_ctx.createCompound(std::string{id}, name, type);

    auto const loc = getSourceLocation(m_config, _xmlNode);
    auto const &file = m_ctx.takeCompoundById(loc);

    m_ctx.takeRelations().add(file, compound);
    return compound;
}

} // namespace Parser
} // namespace Inhale
