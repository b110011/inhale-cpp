#include "src/graph_utils.hpp"

#include "inhale/logging.hpp"

#include <magic_enum.hpp>

namespace Inhale {
namespace Parser {
namespace {

constexpr std::size_t
hash(std::string_view _str) noexcept
{
    if (_str.size() < 4)
    {
        return 0;
    }

    return static_cast<std::size_t>(
        _str[0] | (_str[1] << 8) | (_str[2] << 16) | (_str[3] << 24)
    );
}

} // namespace

Storage::CompoundType
toCompoundType(std::string_view _str)
{
    static_assert(magic_enum::enum_count<Storage::CompoundType>() == 15);
    switch (hash(_str))
    {
        case hash("class"):
            return Storage::CompoundType::Class;

        case hash("concept"):
            return Storage::CompoundType::Concept;

        case hash("define"):
            return Storage::CompoundType::Define;

        case hash("directory"):
            return Storage::CompoundType::Directory;

        case hash("enum"):
            return Storage::CompoundType::Enum;

        // case hash("enumvalue"):
        //     return Storage::CompoundType::EnumValue;

        case hash("file"):
            return Storage::CompoundType::File;

        case hash("function"):
            return Storage::CompoundType::Function;

        case hash("group"):
            return Storage::CompoundType::Group;

        case hash("namespace"):
            return Storage::CompoundType::Namespace;

        case hash("page"):
            return Storage::CompoundType::Page;

        case hash("struct"):
            return Storage::CompoundType::Struct;

        case hash("typedef"):
            return Storage::CompoundType::Typedef;

        case hash("union"):
            return Storage::CompoundType::Union;

        case hash("variable"):
            return Storage::CompoundType::Variable;

        default:
            INHALE_ERROR("Could not parse '{}' compound kind!", _str);
            throw std::runtime_error{"Could not parse compound kind!"};
    }
}

} // namespace Parser
} // namespace Inhale
