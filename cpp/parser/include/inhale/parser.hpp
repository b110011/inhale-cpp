#ifndef __INHALE_PARSER_PARSER_HPP__
#define __INHALE_PARSER_PARSER_HPP__

#include "inhale/parser_fwd.hpp"
#include "inhale/storage_fwd.hpp"

namespace Inhale {
namespace Parser {

void
parse(Storage::Context &_ctx, Config const &_config);

} // namespace Parser
} // namespace Inhale

#endif // __INHALE_PARSER_PARSER_HPP__
