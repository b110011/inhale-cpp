#ifndef __INHALE_PARSERCONFIG_HPP__
#define __INHALE_PARSERCONFIG_HPP__

#include <filesystem>
#include <string>

namespace Inhale {
namespace Parser {

struct Config
{
    std::string const sourcesDir;
    std::filesystem::path const doxygenXmlOutputDir;
};

} // namespace Parser
} // namespace Inhale

#endif // __INHALE_PARSERCONFIG_HPP__
