#include "inhale/parser_config.hpp"
#include "inhale/parser.hpp"
#include "inhale/storage/accessor.hpp"

#include <catch2/catch_test_macros.hpp>

TEST_CASE("Generating random ints", "[doxygen]") {
    auto const version = GENERATE(generateDoxygenVersions());
    INFO("doxygen version: " << version);

    auto pStorage = Inhale::Storage::createStorage();
    Inhale::Parser::Config const config{
        .sourcesDir = "@TODO@",
        .doxygenXmlOutputDir = std::format("@TODO@/{}/@TODO@", version)
    };

    Inhale::Parser::parse(*pStorage, config);

    std::ifstream ifs{"@TODO@"};
}
