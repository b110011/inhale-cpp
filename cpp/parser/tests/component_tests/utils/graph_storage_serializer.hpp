#ifndef __INHALE_PARSER_TESTS_COMPONENTTESTS_UTILS_GRAPHSTORAGESERIALIZER_HPP__
#define __INHALE_PARSER_TESTS_COMPONENTTESTS_UTILS_GRAPHSTORAGESERIALIZER_HPP__

#include "inhale/storage_fwd.hpp"
#include "inhale/storage/context.hpp"

#include <string>

namespace Inhale {
namespace Parser {
namespace Tests {

std::string
serialize(Storage::Context const &_graph);

} // namespace Tests
} // namespace Parser
} // namespace Inhale

#endif // __INHALE_PARSER_TESTS_COMPONENTTESTS_UTILS_GRAPHSTORAGESERIALIZER_HPP__
