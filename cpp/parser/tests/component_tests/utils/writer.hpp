#ifndef __INHALE_PARSER_TESTS_COMPONENTTESTS_UTILS_WRITER_HPP__
#define __INHALE_PARSER_TESTS_COMPONENTTESTS_UTILS_WRITER_HPP__

#include <format>
#include <string>

namespace Inhale {
namespace Parser {
namespace Tests {

class Writer final
{
    static constexpr char k_IndentChar = ' ';
    static constexpr std::size_t k_IndentLength = 2;

public:

    void indent()
    {
        m_indent.append(k_IndentLength, k_IndentChar);
    }

    void unindent()
    {
        m_indent.erase(m_indent.size() - k_IndentLength);
    }

    template<typename... _ArgsT>
    void write(std::format_string<_ArgsT...> _fmt, _ArgsT&&... _args)
    {
        m_content += m_indent;
        std::format_to(std::back_inserter(m_content), std::forward<_ArgsT>(_args)...);
        m_content += '\n';
    }

    std::string own()
    {
        return std::move(m_content);
    }

private:

    std::string m_content;
    std::string m_indent;
}

class IndentationGuard final
{
public:

    explicit IndentationGuard(Writer &_writer)
        : m_writer{_writer}
    {
        m_writer.indent();
    }

    ~IndentationGuard()
    {
        m_writer.unindent();
    }

private:

    Writer &m_writer;
};

inline IndentationGuard
indent(Writer &_writer)
{
    return IndentationGuard{_writer};
}

} // namespace Tests
} // namespace Parser
} // namespace Inhale

#endif // __INHALE_PARSER_TESTS_COMPONENTTESTS_UTILS_WRITER_HPP__
