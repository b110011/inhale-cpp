#include "graph_storage_serializer.hpp"

#include "inhale/storage/compounds/data_structure.hpp"
#include "inhale/storage/compounds/file.hpp"
#include "inhale/storage/compounds/function.hpp"
#include "inhale/storage/compounds/page.hpp"
#include "inhale/storage/context.hpp"
#include "inhale/storage/relations.hpp"

#include "writer.hpp"

namespace Inhale {
namespace Parser {
namespace Tests {
namespace {

void
serialize(Storage::Compound const &_compound, Writer &_writer)
{
    _writer.write("{}", _compound.getId());
    auto const i1 = indent(_writer);

    _writer.write("project: {}", _compound.getProject());
    _writer.write("name: {}", _compound.getName());

    switch (_compound.getType())
    {
        case Storage::CompoundType::Class:
        case Storage::CompoundType::Struct:
            /* code */
            break;

        case Storage::CompoundType::File:
        {
            const auto &file = static_cast<Storage::File const &>(_compound);

            _writer.write("language: {}", file.getLanguage());
            _writer.write("location: {}", file.getSourceLocation());
            break;
        }

        case Storage::CompoundType::Function:
            /* code */
            break;

        case Storage::CompoundType::Page:
        {
            const auto &page = static_cast<Storage::Page const &>(_compound);

            _writer.write("location: {}", page.getSourceLocation());
            _writer.write("title: {}", page.getTitle());
            break;
        }

        default:
            break;
    }
}

void
serializeAllCompounds(Storage::Context const &_graph, Writer &_writer)
{
    for (auto const ct : magic_enum::enum_values<Storage::CompoundType>())
    {
        if (_ctx.getCompoundsCountByType(ct) == 0)
        {
            continue;
        }

        _writer.write("{}", magic_enum::enum_name(ct));
        auto const i1 = indent(_writer);

        _ctx.forEachCompoundByType(
            ct,
            [&_writer](Storage::Compound const &_compound)
            {
                serialize(_compound, _writer);
                return true;
            }
        );
    }
}

void
serializeRelation(Storage::Context const &_graph, Storage::Compound const &_compound, Writer &_writer)
{
    for (auto const ct : magic_enum::enum_values<Storage::CompoundType>())
    {
        _ctx.getRelations().forEachChildByType(
            _compound,
            ct,
            [&_writer](Storage::Compound const &_child)
            {
                _writer.write("{} ({})", _child.getId(), _child.getProject());
                return true;
            }
        );
    }
}

void
serializeAllRelations(Storage::Context const &_graph, Writer &_writer)
{
    _writer.write("RELATIONS");
    auto const i1 = indent(_writer);

    _ctx.forEachCompoundByType(
        Storage::CompoundType::File,
        [&_graph, &_writer](Storage::Compound const &_file)
        {
            serializeRelation(_graph, _file, _writer);
            return true;
        }
    );
}

} // namespace

std::string
serialize(Storage::Context const &_graph)
{
    Writer writer;

    serializeAllCompounds(_graph, writer);
    serializeAllRelations(_graph, writer);

    return writer.own();
}

} // namespace Tests
} // namespace Parser
} // namespace Inhale
