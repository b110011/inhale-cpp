#ifndef __INHALE_BINDINGS_IH_SPHINXLOGGERADAPTER_HPP__
#define __INHALE_BINDINGS_IH_SPHINXLOGGERADAPTER_HPP__

#include "inhale/logging.hpp"

#include <pybind11/pybind11.h>

namespace Inhale {
namespace Bindings {

class SphinxLoggerAdapter final
    : public Logging::Logger
{
public:

    SphinxLoggerAdapter(pybind11::object _obj) noexcept
        : m_obj{std::move(_obj)}
        , m_log{m_obj.attr("log")}
    {}

private:

    void doLog(Logging::Level _level, std::string _message) final
    {
        // ATTENTION(b110011): Keep in sync with python 'logging' library constants.
        const auto level = (static_cast<int>(_level) + 1) * 10;

        m_log(static_cast<int>(_level), std::move(_message));
    }

private:

    pybind11::object m_obj;
    pybind11::detail::str_attr_accessor m_log;
};

} // namespace Bindings
} // namespace Inhale

#endif // __INHALE_BINDINGS_IH_SPHINXLOGGERADAPTER_HPP__
