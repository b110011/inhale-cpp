#include "inhale/storage/accessor.hpp"
#include "inhale/logging.hpp"
#include "inhale/parser_config.hpp"
#include "inhale/parser.hpp"
#include "inhale/render_config.hpp"
#include "inhale/render.hpp"

#include "ih/sphinx_logger_adapter.hpp"

#include <pybind11/pybind11.h>

namespace py = pybind11;

static void
bindLogger(py::module _m)
{
    _m.def(
        "set_logger",
        [](py::object _obj)
        {
            using Logger = Inhale::Bindings::SphinxLoggerAdapter;
            Inhale::Logging::setLogger(std::make_unique<Logger>(std::move(_obj)));
        }
    );
    _m.def("reset_logger", []{ Inhale::Logging::setLogger(nullptr); });
}

static void
bindGraph(py::module _m)
{
    py::class_<Inhale::Storage::Context, std::unique_ptr<Inhale::Storage::Context>>(_m, "Graph")
        .def(py::init([]() { return Inhale::Storage::createContext(); }));
        // .def("set_current_project", &Inhale::Storage::Context::setCurrentProject);
}

static void
bindDoxygen(py::module _m)
{
    py::class_<Inhale::Parser::Config>(_m, "ParserConfig")
        .def(py::init<std::string, std::filesystem::path>());

    _m.def("parse_doxygen_output", &Inhale::Parser::parse);
}

static void
bindRst(py::module _m)
{
    py::class_<Inhale::Render::Config>(_m, "RenderConfig");

    _m.def("render_rst_files", &Inhale::Render::render);
}

// Clang-Tidy: We can do nothing here as this is 3rd-party library API.
// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables, cppcoreguidelines-pro-bounds-pointer-arithmetic, cppcoreguidelines-pro-type-vararg, hicpp-vararg)
PYBIND11_MODULE(backend, m)
{
    bindLogger(m);
    bindGraph(m);
    bindDoxygen(m);
    bindRst(m);
}
