#include "inhale/storage/accessor.hpp"
#include "inhale/logging.hpp"
#include "inhale/parser_config.hpp"
#include "inhale/parser.hpp"
#include "inhale/render_config.hpp"
#include "inhale/render.hpp"

#include <Python.h>

// https://github.com/ethereum/serpent-go/blob/master/serpent/pyserpent.cpp
// https://stackoverflow.com/questions/48786693
// https://llllllllll.github.io/c-extension-tutorial/index.html

struct StringBuffer
{
    char const *m_pData = NULL;
    Py_ssize_t m_length = 0;
};

/* -------------------------------------------------------------------------- *
 * Logging                                                                    *
 * -------------------------------------------------------------------------- */

class PySphinxLoggerAdapter final
    : public Inhale::Logging::Logger
{
public:

    PySphinxLoggerAdapter(PyObject *_pObj, PyObject *_pLogMethod) noexcept
        : m_pObj{_pObj}
        , m_pLogMethod{_pLogMethod}
    {
        Py_IncRef(m_pObj);
        Py_IncRef(m_pLogMethod);
    }

    ~PySphinxLoggerAdapter() noexcept
    {
        Py_DecRef(m_pLogMethod);
        Py_DecRef(m_pObj);
    }

private:

    void doLog(Logging::Level _level, std::string _message) noexcept final
    {
        // ATTENTION(b110011): Keep in sync with python 'logging' library constants.
        auto const level = (static_cast<int>(_level) + 1) * 10;

        PyObject_CallFunction(m_pLogMethod, "is#", level, _message.c_str(), _message.size());
    }

private:

    PyObject *m_pObj;
    PyObject *m_pLogMethod;
};

static PyObject *
set_logger(PyObject * Py_UNUSED(_pSelf), PyObject *_pLogger)
{
    PyObject *pLogMethod = PyObject_GetAttrString(_pLogger, "log");
    if (pLogMethod == NULL)
    {
        return NULL;
    }

    Inhale::Logging::setLogger(
        std::make_unique<PySphinxLoggerAdapter>(_pLogger, pLogMethod)
    );

    return Py_RETURN_NONE;
}

static PyObject *
reset_logger(PyObject * Py_UNUSED(_pSelf), PyObject * Py_UNUSED(_pArgs))
{
    Inhale::Logging::setLogger(nullptr);
    return Py_RETURN_NONE;
}

/* -------------------------------------------------------------------------- *
 * Storage                                                                    *
 * -------------------------------------------------------------------------- */

typedef struct {
    PyObject_HEAD
    Inhale::Storage::Context *pObj;
} PyContext;

static int
PyContext_Init(
    PyContext * Py_UNUSED(_pSelf),
    PyObject * Py_UNUSED(_pArgs),
    PyObject * Py_UNUSED(_pKwargs)
)
{
    _pSelf->pObj = Inhale::Storage::createContext().release();

    return 0;
}

static void
PyContext_Dealloc(PyContext *_pSelf)
{
    delete _pSelf->pObj;
    Py_TYPE(_pSelf)->tp_free(_pSelf);
}

static PyObject *
PyContext_SetCurrentProject(PyContext * Py_UNUSED(_pSelf), PyObject *_pArgs)
{
    StringBuffer b;
    if (!PyArg_ParseTuple(
        _pArgs, "s#:_backend.Context.set_current_project",
        &b.m_pData,
        &b.m_length
    ))
    {
        return NULL;
    }

    _pSelf->pObj->setCurrentProject({b.m_pData, b.m_length});

    return Py_RETURN_NONE;
}

static PyMethodDef s_pyContextMethods[] = {
    {"set_current_project", PyContext_SetCurrentProject, METH_VARARGS, "" },
    {NULL, NULL, 0, NULL}  // Sentinel
};

static PyTypeObject s_pyContextType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "_backend.Context",                 // tp_name
    sizeof(Inhale::Storage::Context *), // tp_basicsize
    0,                                  // tp_itemsize
    PyContext_Dealloc,                  // tp_dealloc
    0,                                  // tp_vectorcall_offset
    NULL,                               // tp_getattr
    NULL,                               // tp_setattr
    NULL,                               // tp_as_async
    NULL,                               // tp_repr
    NULL,                               // tp_as_number
    NULL,                               // tp_as_sequence
    NULL,                               // tp_as_mapping
    NULL,                               // tp_hash
    NULL,                               // tp_call
    NULL,                               // tp_str
    NULL,                               // tp_getattro
    NULL,                               // tp_setattro
    NULL,                               // tp_as_buffer
    Py_TPFLAGS_DEFAULT,                 // tp_flags
    NULL,                               // tp_doc
    NULL,                               // tp_traverse
    NULL,                               // tp_clear
    NULL,                               // tp_richcompare
    0,                                  // tp_weaklistoffset
    NULL,                               // tp_iter
    NULL,                               // tp_iternext
    s_pyContextMethods,                 // tp_methods
    NULL,                               // tp_members
    NULL,                               // tp_getset
    NULL,                               // tp_base
    NULL,                               // tp_dict
    NULL,                               // tp_descr_get
    NULL,                               // tp_descr_set
    0,                                  // tp_dictoffset
    PyContext_Init,                     // tp_init
    NULL,                               // tp_alloc
    PyType_GenericNew,                  // tp_new
    NULL,                               // tp_free
};

/* -------------------------------------------------------------------------- *
 * Parser                                                                     *
 * -------------------------------------------------------------------------- */

typedef struct {
    PyObject_HEAD
    Inhale::Parser::Config *pObj;
} PyParserConfig;

static char const* s_pParserConfigKeywords[] = {"sources_dir", "doxygen_xml_output_dir", NULL};

static int
PyParserConfig_Init(PyParserConfig * Py_UNUSED(_pSelf), PyObject *_pArgs, PyObject *_pKwargs)
{
    StringBuffer b1, b2;
    if (!PyArg_ParseTupleAndKeywords(
        _pArgs,
        _pKwargs,
        "s#s#:_backend.ParserConfig.__init__",
        s_pParserConfigKeywords,
        &b1.m_pData,
        &b1.m_length,
        &b2.m_pData,
        &b2.m_length,
    ))
    {
        return NULL;
    }

    _pSelf->pObj = new Inhale::Parser::Config{
        .sourcesDir = {b1.m_pData, b1.m_length},
        .doxygenXmlOutputDir = {b2.m_pData, b2.m_length}
    };

    return 0;
}

static void
PyParserConfig_Dealloc(PyParserConfig *_pSelf)
{
    delete _pSelf->pObj;
    Py_TYPE(_pSelf)->tp_free(_pSelf);
}

static PyTypeObject s_pyParserConfigType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "_backend.ParserConfig",            // tp_name
    sizeof(Inhale::Parser::Config *),   // tp_basicsize
    0,                                  // tp_itemsize
    PyParserConfig_Dealloc,             // tp_dealloc
    0,                                  // tp_vectorcall_offset
    NULL,                               // tp_getattr
    NULL,                               // tp_setattr
    NULL,                               // tp_as_async
    NULL,                               // tp_repr
    NULL,                               // tp_as_number
    NULL,                               // tp_as_sequence
    NULL,                               // tp_as_mapping
    NULL,                               // tp_hash
    NULL,                               // tp_call
    NULL,                               // tp_str
    NULL,                               // tp_getattro
    NULL,                               // tp_setattro
    NULL,                               // tp_as_buffer
    Py_TPFLAGS_DEFAULT,                 // tp_flags
    NULL,                               // tp_doc
    NULL,                               // tp_traverse
    NULL,                               // tp_clear
    NULL,                               // tp_richcompare
    0,                                  // tp_weaklistoffset
    NULL,                               // tp_iter
    NULL,                               // tp_iternext
    NULL,                               // tp_methods
    NULL,                               // tp_members
    NULL,                               // tp_getset
    NULL,                               // tp_base
    NULL,                               // tp_dict
    NULL,                               // tp_descr_get
    NULL,                               // tp_descr_set
    0,                                  // tp_dictoffset
    PyParserConfig_Init,                // tp_init
    NULL,                               // tp_alloc
    PyType_GenericNew,                  // tp_new
    NULL,                               // tp_free
};

static PyObject *
parse_doxygen_output(PyObject * Py_UNUSED(_pSelf), PyObject *_pArgs)
{
    PyObject *pContext = NULL;
    PyObject *pConfig = NULL;

    if (!PyArg_ParseTuple(
        _pArgs,
        "O!O!:_backend.parse_doxygen_output",
        &pContext,
        &s_pyContextType,
        &pConfig,
        &s_pyParserConfigType
    ))
    {
        return NULL;
    }

    try
    {
        Inhale::Parse::parse(
            *(((PyContext *)pContext)->pObj),
            *(((PyParserConfig *)pConfig)->pObj)
        );
    }
    catch(std::exception const &_e)
    {
        PyErr_SetString(PyExc_RuntimeError, _e.c_str());
        return NULL;
    }

    return Py_RETURN_NONE;
}

/* -------------------------------------------------------------------------- *
 * Render                                                                     *
 * -------------------------------------------------------------------------- */

typedef struct {
    PyObject_HEAD
    Inhale::Render::Config *pObj;
} PyRenderConfig;


static int
PyRenderConfig_Init(PyRenderConfig * Py_UNUSED(_pSelf), PyObject *_pArgs, PyObject *_pKwargs)
{
    // TODO: Parse
    _pSelf->pObj = new Inhale::Render::Config{};

    return 0;
}

static void
PyRenderConfig_Dealloc(PyRenderConfig *_pSelf)
{
    delete _pSelf->pObj;
    Py_TYPE(_pSelf)->tp_free(_pSelf);
}

static PyTypeObject s_pyRenderConfigType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "_backend.RenderConfig",            // tp_name
    sizeof(Inhale::Render::Config *),   // tp_basicsize
    0,                                  // tp_itemsize
    PyRenderConfig_Dealloc,             // tp_dealloc
    0,                                  // tp_vectorcall_offset
    NULL,                               // tp_getattr
    NULL,                               // tp_setattr
    NULL,                               // tp_as_async
    NULL,                               // tp_repr
    NULL,                               // tp_as_number
    NULL,                               // tp_as_sequence
    NULL,                               // tp_as_mapping
    NULL,                               // tp_hash
    NULL,                               // tp_call
    NULL,                               // tp_str
    NULL,                               // tp_getattro
    NULL,                               // tp_setattro
    NULL,                               // tp_as_buffer
    Py_TPFLAGS_DEFAULT,                 // tp_flags
    NULL,                               // tp_doc
    NULL,                               // tp_traverse
    NULL,                               // tp_clear
    NULL,                               // tp_richcompare
    0,                                  // tp_weaklistoffset
    NULL,                               // tp_iter
    NULL,                               // tp_iternext
    NULL,                               // tp_methods
    NULL,                               // tp_members
    NULL,                               // tp_getset
    NULL,                               // tp_base
    NULL,                               // tp_dict
    NULL,                               // tp_descr_get
    NULL,                               // tp_descr_set
    0,                                  // tp_dictoffset
    PyRenderConfig_Init,                // tp_init
    NULL,                               // tp_alloc
    PyType_GenericNew,                  // tp_new
    NULL,                               // tp_free
};

static PyObject *
render_rst_files(PyObject * Py_UNUSED(_pSelf), PyObject *_pArgs)
{
    PyObject *pContext = NULL;
    PyObject *pConfig = NULL;

    if (!PyArg_ParseTuple(
        _pArgs,
        "O!O!:_backend.render_rst_files",
        &pContext,
        &s_pyContextType,
        &pConfig,
        &s_pyRenderConfigType
    ))
    {
        return NULL;
    }

    try
    {
        Inhale::Render::render(
            *(((PyContext *)pContext)->pObj),
            *(((PyRenderConfig *)pConfig)->pObj)
        );
    }
    catch(std::exception const &_e)
    {
        PyErr_SetString(PyExc_RuntimeError, _e.c_str());
        return NULL;
    }

    return Py_RETURN_NONE;
}

/* -------------------------------------------------------------------------- *
 * Module                                                                     *
 * -------------------------------------------------------------------------- */

static PyMethodDef s_pyBackendModuleMethods[] = {
    {"set_logger", set_logger, METH_O, NULL},
    {"reset_logger", reset_logger, METH_NOARGS, NULL},
    {"parse_doxygen_output", parse_doxygen_output, METH_VARARGS, NULL},
    {"render_rst_files", render_rst_files, METH_VARARGS, NULL},
    {NULL, NULL, 0, NULL}  // Sentinel
};

static PyModuleDef s_pyBackendModuleDef = {
    PyModuleDef_HEAD_INIT,
    "_backend",
    "A C++ implementation of backend for Inhale Sphinx extention",
    -1,
    s_pyBackendModuleMethods,
    NULL,
    NULL,
    NULL,
    NULL
};

PyMODINIT_FUNC
PyInit_backend()
{
    if (PyType_Ready(&s_pyContextType) < 0)
    {
        return NULL;
    }

    if (PyType_Ready(&s_pyParserConfigType) < 0)
    {
        return NULL;
    }

    if (PyType_Ready(&s_pyRenderConfigType) < 0)
    {
        return NULL;
    }

    PyObject *pPyModule = PyModule_Create(&s_pyBackendModuleDef);
    if (!pPyModule)
    {
        return NULL;
    }

    return pPyModule;
}
