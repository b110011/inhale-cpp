#ifndef __INHALE_STORAGEFWD_HPP__
#define __INHALE_STORAGEFWD_HPP__

#include "inhale/storage/compound_type.hpp"

#include <functional>

namespace Inhale {
namespace Storage {

class Compound;

class DataStructure;
class Directory;
class File;
class Function;
class Page;

class Context;
class Inheritances;
class Relations;

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGEFWD_HPP__
