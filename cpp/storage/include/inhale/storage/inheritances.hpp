#ifndef __INHALE_STORAGE_INHERITANCES_HPP__
#define __INHALE_STORAGE_INHERITANCES_HPP__

#include <cstdint>
#include <functional>

namespace Inhale {
namespace Storage {

class Compound;

/**
 * @brief An enumeration type that represents all C++ inheritance types.
 */
enum class InheritanceType : std::uint32_t
{
    /// Public inheritance.
    Public = 0,

    /// Virtual public inheritance.
    VirtualPublic,

    /// Protected inheritance.
    Protected,

    /// Virtual protected inheritance.
    VirtualProtected,

    /// Private inheritance.
    Private,

    /// Virtual private inheritance.
    VirtualPrivate,
};

/**
 * @brief A simple storage that collects inheritances information of all C++
 *     @c class s and @c struct s.
 *
 * @par Thread Safety
 *   - @e Distinct @e objects: Safe.
 *   - @e Shared @e objects: Unsafe.
 *
 * @note @c class and @c struct are used interchangable here.
 */
class Inheritances
{
public:

    /**
     * @brief Default virtual destructor.
     */
    virtual ~Inheritances() = default;

    /**
     * @brief Add information about new inheritance between two classes.
     *
     * @param _base a base class.
     * @param _derivative a class that derives from a @p _base class.
     * @param _inheritanceType an inheritance type between @p _derivative and
      *     @p _base classes.
     *
     * @note If information about inheritance exists, nothing will be performed.
     */
    virtual void add(
        Compound const &_base,
        Compound const &_derivative,
        InheritanceType _inheritanceType
    ) = 0;

    [[nodiscard]]
    virtual std::size_t getBaseTypesCount(Compound const &_type) const = 0;

    [[nodiscard]]
    virtual std::size_t getDerivedTypesCount(Compound const &_type) const = 0;

    /**
     * @brief The definition of the function object to be used in the
     *     @c forEach -like methods.
     */
    using Callback = std::function<bool(
        Compound const & /*_type*/,
        InheritanceType /*_inheritanceType*/
    )>;

    /**
     * @brief Applies the given function object to each base of a specified
     *     class until the criteria is satisfied.
     *
     * @param _derivative a class for which bases will be looked for.
     * @param _callback function object of type @ref Callback, to be applied to
     *     each parent of an compound.
     *
     * @return The result of supplied function object if graph contains at lease
     *     one child for a specified compound, otherwise @c true .
     */
    virtual bool forEachBaseType(Compound const &_derivative, Callback _callback) const = 0;

    /**
     * @brief Applies the given function object to each class that is derived from
     *     @p _base class.
     *
     * @param _base a base class for which derivations will be looked for.
     * @param _callback function object of type @ref Callback, to be applied to
     *     each parent of an compound.
     *
     * @return The result of supplied function object if graph contains at lease
     *     one parent for a specified compound, otherwise @c true .
     */
    virtual bool forEachDerivedType(Compound const &_base, Callback _callback) const = 0;
};

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_INHERITANCES_HPP__
