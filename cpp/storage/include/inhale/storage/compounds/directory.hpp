#ifndef __INHALE_STORAGE_COMPOUNDS_DIRECTORY_HPP__
#define __INHALE_STORAGE_COMPOUNDS_DIRECTORY_HPP__

#include "inhale/storage/compound.hpp"

#include <string>

namespace Inhale {
namespace Storage {

class Directory
    : public Compound
{
public:

    virtual void setPath(std::string _path) = 0;

    [[nodiscard]]
    virtual std::string_view getPath() const = 0;
};

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_COMPOUNDS_DIRECTORY_HPP__
