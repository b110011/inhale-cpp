#ifndef __INHALE_STORAGE_COMPOUNDS_FUNCTION_HPP__
#define __INHALE_STORAGE_COMPOUNDS_FUNCTION_HPP__

#include "inhale/storage/compound.hpp"

#include <functional>
#include <string_view>
#include <string>
#include <variant>

namespace Inhale {
namespace Storage {

class Function
    : public Compound
{
public:

    virtual void setReturnType(std::string _type) = 0;

    [[nodiscard]]
    virtual std::string_view getReturnType() const = 0;


    virtual void addParam(std::string _param) = 0;

    [[nodiscard]]
    virtual std::size_t getParamsCount() const = 0;

    using ParamCallback = std::function<bool(std::string_view const & /*_param*/)>;

    virtual bool forEachParam(ParamCallback _callback) const = 0;


    virtual void addTemplateParam(std::string _param) = 0;

    [[nodiscard]]
    virtual std::size_t getTemplateParamsCount() const = 0;

    using TemplateParamCallback = std::function<bool(std::string_view const & /*_param*/)>;

    virtual bool forEachTemplateParam(TemplateParamCallback _callback) const = 0;
};

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_COMPOUNDS_FUNCTION_HPP__
