#ifndef __INHALE_STORAGE_COMPOUNDS_DATASTRUCTURE_HPP__
#define __INHALE_STORAGE_COMPOUNDS_DATASTRUCTURE_HPP__

#include "inhale/storage/compound.hpp"

#include <functional>
#include <string>
#include <variant>

namespace Inhale {
namespace Storage {

struct TemplateParam final
{
    const std::variant<CompoundConstRef, std::string> type;
    const std::string declaredName;
    const std::string definedName;
};

class DataStructure
    : public Compound
{
public:

    virtual void addTemplateParam(TemplateParam _param) = 0;

    [[nodiscard]]
    virtual std::size_t getTemplateParamsCount() const = 0;

    using Callback = std::function<bool(TemplateParam const & /*_param*/)>;

    virtual bool forEachTemplateParam(Callback _callback) const = 0;
};

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_COMPOUNDS_DATASTRUCTURE_HPP__
