#ifndef __INHALE_STORAGE_RELATIONS_HPP__
#define __INHALE_STORAGE_RELATIONS_HPP__

#include "inhale/storage/compound_type.hpp"

#include <functional>

namespace Inhale {
namespace Storage {

class Compound;

class Relations
{
public:

    /**
     * @brief Default virtual destructor.
     */
    virtual ~Relations() = default;

    /**
     * @brief Add a new edge between two supplied vertices, a.k.a @ref Compound.
     *
     * If an edge already exists, then nothing will be performed.
     *
     * @param _parent a parent vertex of new edge.
     * @param _child a child vertex of new edge.
     */
    virtual void add(Compound const &_parent, Compound const &_child) = 0;

    virtual std::size_t getChildrenCountByType(
        Compound const &_compound,
        CompoundType _type
    ) const = 0;

    virtual std::size_t getParentsCountByType(
        Compound const &_compound,
        CompoundType _type
    ) const = 0;

    /**
     * @brief The definition of the function object to be used in the
     *     @c forEach -like methods.
     */
    using Callback = std::function<bool(Compound const & /*_compound*/)>;

    /**
     * @brief Applies the given function object to each child of a specified
     *     compound until the criteria is satisfied.
     *
     * @param _compound a compound for which children will be looked for.
     * @param _callback function object of type @ref Callback, to be applied to
     *     each parent of a compound.
     *
     * @return The result of supplied function object if graph contains at lease
     *     one child for a specified compound, otherwise @c true .
     */
    virtual bool forEachChildByType(
        Compound const &_compound,
        CompoundType _type,
        Callback _callback
    ) const = 0;

    /**
     * @brief Applies the given function object to each parent of a specified
     *     compound until the criteria is satisfied.
     *
     * @param _compound a compound for which parents will be looked for.
     * @param _callback function object of type @ref Callback, to be applied to
     *     each parent of a compound.
     *
     * @return The result of supplied function object if graph contains at lease
     *     one parent for a specified compound, otherwise @c true .
     */
    virtual bool forEachParentByType(
        Compound const &_compound,
        CompoundType _type,
        Callback _callback
    ) const = 0;
};

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_RELATIONS_HPP__
