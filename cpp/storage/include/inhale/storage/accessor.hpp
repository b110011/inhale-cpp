#ifndef __INHALE_STORAGE_ACCESSOR_HPP__
#define __INHALE_STORAGE_ACCESSOR_HPP__

#include "inhale/storage/context.hpp"

#include <memory>

namespace Inhale {
namespace Storage {

/**
 * @brief Creates a new storage context object.
 *
 * @return A pointer to a new object if it was created, @c nullptr otherwise.
 */
[[nodiscard]]
std::unique_ptr<Context> createContext();

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_ACCESSOR_HPP__
