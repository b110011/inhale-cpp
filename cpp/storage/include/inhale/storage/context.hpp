#ifndef __INHALE_STORAGE_CONTEXT_HPP__
#define __INHALE_STORAGE_CONTEXT_HPP__

#include "inhale/storage/compound_type.hpp"

#include <functional>
#include <string_view>
#include <string>

namespace Inhale {
namespace Storage {

class Compound;
class Inheritances;
class Relations;

class Context
{
public:

    /**
     * @brief Default virtual destructor.
     */
    virtual ~Context() = default;

    // virtual void setCurrentProject(std::string _project) = 0;

    [[nodiscard]]
    virtual Compound &createCompound(
        std::string _id,
        std::string _name,
        CompoundType _type
    ) = 0;

    // virtual void reloc(Compound const &_compound) = 0;

    /**
     * @name Finds a compound with name equivalent to @p _name .
     *
     * @param _name a name to search for.
     *
     * @return Pointer to a compound, @c nullptr otherwise.
     *
     * @warning Returns a non-owning pointer to a compound.
     */
    //@{
    [[nodiscard]]
    virtual Compound const &getCompoundById(std::string_view _id) const = 0;

    [[nodiscard]]
    virtual Compound &takeCompoundById(std::string_view _id) = 0;
    //@}

    [[nodiscard]]
    virtual std::size_t getCompoundsCountByType(CompoundType _type) const = 0;

    /**
     * @brief The definition of the function object to be used in the
     *     @c forEach -like methods.
     */
    template<typename _T>
    using Callback = std::function<bool(_T & /*_compound*/)>;

    /**
     * @name Applies the given function object to each child of a specified
     *     compound until the criteria is satisfied.
     *
     * @param _compound an compound for which children will be looked for.
     * @param _callback function object of type @ref Callback, to be applied to
     *     each parent of an compound.
     *
     * @return The result of supplied function object if graph contains at lease
     *     one child for a specified compound, otherwise @c true .
     */
    //@{
    virtual bool forEachCompoundByType(CompoundType _type, Callback<Compound const> _callback) const = 0;

    virtual bool forEachCompoundByType(CompoundType _type, Callback<Compound> _callback) = 0;
    //@}

    /**
     * @brief Returns a const reference to the relations graph of all file compounds.
     *
     * @return A const reference to the relations graph of all file compounds.
     *
     * @note This is special graph that stores what a specific file includes and
     *     what files include that specific file.
     */
    [[nodiscard]]
    virtual Relations const &getFileRelations() const = 0;

    /**
     * @brief Returns a reference to the relations graph of all file compounds.
     *
     * @return A reference to the relations graph of all file compounds.
     *
     * @note This is special graph that stores what a specific file includes and
     *     what files include that specific file.
     */
    [[nodiscard]]
    virtual Relations &takeFileRelations() = 0;

    /**
     * @brief Returns a const reference to the relations graph of all compounds.
     *
     * @return A const reference to the relations graph of all compounds.
     */
    [[nodiscard]]
    virtual Inheritances const &getInheritances() const = 0;

    /**
     * @brief Returns a reference to the relations graph of all compounds.
     *
     * @return A reference to the relations graph of all compounds.
     */
    [[nodiscard]]
    virtual Inheritances &takeInheritances() = 0;

    /**
     * @brief Returns a const reference to the relations graph of all compounds.
     *
     * @return A const reference to the relations graph of all compounds.
     */
    [[nodiscard]]
    virtual Relations const &getRelations() const = 0;

    /**
     * @brief Returns a reference to the relations graph of all compounds.
     *
     * @return A reference to the relations graph of all compounds.
     */
    [[nodiscard]]
    virtual Relations &takeRelations() = 0;
};

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_CONTEXT_HPP__
