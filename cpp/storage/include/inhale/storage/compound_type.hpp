#ifndef __INHALE_STORAGE_COMPOUNDTYPE_HPP__
#define __INHALE_STORAGE_COMPOUNDTYPE_HPP__

#include <cstdint>

namespace Inhale {
namespace Storage {

/**
 * @brief An enumeration type that represents all doxygen compound types.
 */
enum class CompoundType : std::uint32_t
{
    /**
     * @brief A compound that represents [class][1] type.
     *
     * [1]: https://en.wikipedia.org/wiki/Class_(computer_programming)
     */
    Class = 0,

    /**
     * @brief A compound that represents C++ [concept][1].
     *
     * [1]: https://en.cppreference.com/w/cpp/language/constraints
     */
    Concept,

    /**
     * @brief A compound that represents [@c #define macro directive][1].
     *
     * [1]: https://en.cppreference.com/w/cpp/preprocessor/replace
     */
    Define,

    Directory,
    Enum,
    EnumValue,
    File,
    Function,
    Group,
    Namespace,
    Page,
    Struct,
    Typedef,
    Union,
    Variable
};

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_COMPOUNDTYPE_HPP__
