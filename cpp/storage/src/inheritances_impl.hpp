#ifndef __INHALE_STORAGE_SRC_INHERITANCESIMPL_HPP__
#define __INHALE_STORAGE_SRC_INHERITANCESIMPL_HPP__

#include "inhale/storage/inheritances.hpp"

#include "inhale/storage/compound.hpp"

#include "src/compound_utils.hpp"

#include <unordered_map>

namespace Inhale {
namespace Storage {

class InheritancesImpl final
    : public Inheritances
{
public:

    void add(Compound const &_base, Compound const &_derivative, InheritanceType _type) final;


    std::size_t getBaseTypesCount(Compound const &_type) const noexcept final;

    std::size_t getDerivedTypesCount(Compound const &_type) const noexcept final;


    bool forEachBaseType(Compound const &_derivative, Callback _callback) const final;

    bool forEachDerivedType(Compound const &_base, Callback _callback) const final;

private:

    using Edge = std::pair<CompoundConstRef, InheritanceType>;

    struct EdgeComparator final
    {
        bool operator()(Edge const &_lhs, Edge const &_rhs) const;
    };

    using Relations = std::unordered_map<
        CompoundConstRef,
        std::set<Edge, EdgeComparator>,
        CompoundHasher,
        CompoundSimpleComparator
    >;

private:

    Relations m_bases;
    Relations m_derivatives;
};

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_SRC_INHERITANCESIMPL_HPP__
