#include "src/compound_utils.hpp"

#include "inhale/storage/compounds/file.hpp"
#include "inhale/storage/compounds/page.hpp"

#include <cctype>
#include <ranges>

namespace Inhale {
namespace Storage {
namespace {

// std::string_view
// getCorrectId(Compound const &_compound)
// {
//     std::string_view compoundData;
//     switch (_compound.getType())
//     {
//         case CompoundType::File:
//             return static_cast<File const &>(_compound).getSourceLocation();

//         case CompoundType::Page:
//             return static_cast<Page const &>(_compound).getSourceLocation();

//         default:
//             return _compound.getId();
//     }
// }

bool
iequal(char _lhs, char _rhs) noexcept
{
    return std::tolower(static_cast<unsigned char>(_lhs)) ==
           std::tolower(static_cast<unsigned char>(_rhs));
}

bool
iequals(std::string_view _lhs, std::string_view _rhs) noexcept
{
    return std::ranges::equal(_lhs, _rhs, iequal);
}

using Type = CompoundType;

} // namespace

std::size_t
CompoundHasher::operator()(std::string_view _data) const noexcept
{
    return std::hash<std::string_view>{}(_data);
}

std::size_t
CompoundHasher::operator()(Compound const &_compound) const
{
    // return (*this)(getCorrectId(_compound));
    return (*this)(_compound.getId());
}

bool
CompoundSimpleComparator::operator()(std::string_view _data, Compound const &_compound) const
{
    // return getCorrectId(_compound) == _data;
    return _compound.getId() == _data;
}

bool
CompoundSimpleComparator::operator()(Compound const &_lhs, Compound const &_rhs) const
{
    // return (_lhs.getType() == _rhs.getType()) && (getCorrectId(_lhs) == getCorrectId(_rhs));
    return _lhs.getId() == _rhs.getId();
}

bool
CompoundComparator::operator()(Compound const &_lhs, Compound const &_rhs) const
{
    auto const lhsType = _lhs.getType();
    auto const rhsType = _rhs.getType();

    if (lhsType == rhsType)
    {
        if (lhsType != Type::Page)
        {
            return iequals(_lhs.getName(), _rhs.getName());
        }

        /* Arbitrarily stuff "indexpage" refid to the front. As doxygen presents
            things, it shows up last, but it does not matter since the sort we
            really care about will be with lists that do NOT have indexpage in
            them (for creating the page view hierarchy).
         */
        if (_lhs.getId() == "indexpage")
        {
            return true;
        }

        if (_rhs.getId() == "indexpage")
        {
            return false;
        }

        // TODO(b110011): This is a big deviation in behavior from python code.
        //   For more info look at 'ExhaleNode.__lt__'.
        return _lhs.getId() < _rhs.getId();
    }

    if (lhsType == Type::Class || lhsType == Type::Struct)
    {
        if (rhsType != Type::Class && rhsType != Type::Struct)
        {
            return true;
        }

        if (lhsType == Type::Struct && rhsType == Type::Class)
        {
            return true;
        }

        if (lhsType == Type::Class && rhsType == Type::Struct)
        {
            return false;
        }

        return iequals(_lhs.getName(), _rhs.getName());
    }

    return lhsType < rhsType;
}

} // namespace Storage
} // namespace Inhale
