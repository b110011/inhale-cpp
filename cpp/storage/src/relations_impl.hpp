#ifndef __INHALE_STORAGE_SRC_RELATIONSIMPL_HPP__
#define __INHALE_STORAGE_SRC_RELATIONSIMPL_HPP__

#include "inhale/storage/relations.hpp"

#include "inhale/storage/compound.hpp"

#include "src/compound_utils.hpp"

#include <magic_enum.hpp>

#include <array>
#include <unordered_map>
#include <set>

namespace Inhale {
namespace Storage {

class RelationsImpl final
    : public Relations
{
public:

    void add(Compound const &_parent, Compound const &_child) final;


    std::size_t getChildrenCountByType(
        Compound const &_compound,
        CompoundType _type
    ) const noexcept final;

    std::size_t getParentsCountByType(
        Compound const &_compound,
        CompoundType _type
    ) const noexcept final;


    bool forEachChildByType(
        Compound const &_compound,
        CompoundType _type,
        Callback _callback
    ) const final;

    bool forEachParentByType(
        Compound const &_compound,
        CompoundType _type,
        Callback _callback
    ) const final;

private:

    using Compounds = std::unordered_map<
        CompoundConstRef,
        CompoundsByType<CompoundConstRef>,
        CompoundHasher,
        CompoundSimpleComparator
    >;

    Compounds m_children;
    Compounds m_parents;
};

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_SRC_RELATIONSIMPL_HPP__
