#ifndef __INHALE_STORAGE_SRC_CONTEXTIMPL_HPP__
#define __INHALE_STORAGE_SRC_CONTEXTIMPL_HPP__

#include "inhale/storage/context.hpp"

#include "inhale/storage/compound.hpp"

#include "src/compound_utils.hpp"

#include <magic_enum.hpp>

#include <array>
#include <memory>
#include <set>
#include <unordered_map>
#include <vector>

namespace Inhale {
namespace Storage {

class ContextImpl final
    : public Context
{
public:

    ContextImpl();

    ~ContextImpl() final;


    // void setCurrentProject(std::string _project) final;


    Compound &createCompound(std::string _id, std::string _name, CompoundType _type) final;

    // void reloc(Compound const &_compound) final;


    std::size_t getCompoundsCountByType(CompoundType _type) const noexcept final;


    Compound const &getCompoundById(std::string_view _id) const final;

    Compound &takeCompoundById(std::string_view _id) final;


    bool forEachCompoundByType(CompoundType _type, Callback<Compound const> _callback) const final;

    bool forEachCompoundByType(CompoundType _type, Callback<Compound> _callback) final;


    Relations const &getFileRelations() const noexcept final;

    Relations &takeFileRelations() noexcept final;


    Inheritances const &getInheritances() const noexcept final;

    Inheritances &takeInheritances() noexcept final;


    Relations const &getRelations() const noexcept final;

    Relations &takeRelations() noexcept final;

private:

    // std::string_view getCurrentProject() const;

    // template<typename _CompoundImplT, typename... _ArgsT>
    // std::unique_ptr<_CompoundImplT> makeCompound(
    //     std::string _id,
    //     std::string _name,
    //     _ArgsT &&..._type
    // );

private:

    // std::vector<std::string> m_projects;

    std::unordered_map<std::string_view, std::unique_ptr<Compound>> m_compounds;
    CompoundsByType<CompoundRef> m_compoundsByType;

    std::unique_ptr<Relations> m_pFileRelations;
    std::unique_ptr<Inheritances> m_pInheritances;
    std::unique_ptr<Relations> m_pRelations;
};

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_SRC_CONTEXTIMPL_HPP__
