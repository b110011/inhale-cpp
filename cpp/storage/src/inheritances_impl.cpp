#include "src/inheritances_impl.hpp"

#include "inhale/logging.hpp"

namespace Inhale {
namespace Storage {

void
InheritancesImpl::add(Compound const &_base, Compound const &_derivative, InheritanceType _type)
{
    if (&_base == &_derivative)
    {
        INHALE_ERROR("Tried to create an inheritance loop for {}!", _base);
        throw std::runtime_error{"Detected a loop!"};
    }

    if (m_bases[_base].emplace(_derivative, _type).second)
    {
        m_derivatives[_derivative].emplace(_base, _type);
    }

    // TODO(b110011): Throw an exception if edge already exists?
}

template<typename _Container>
std::size_t
getCount(_Container const &_container, Compound const &_compound)
{
    auto it = _container.find(_compound);
    if (it != _container.end())
    {
        return it->second.size();
    }

    return 0;
}

std::size_t
InheritancesImpl::getBaseTypesCount(Compound const &_type) const noexcept
{
    return getCount(m_bases, _type);
}

std::size_t
InheritancesImpl::getDerivedTypesCount(Compound const &_type) const noexcept
{
    return getCount(m_derivatives, _type);
}

template<typename _Container>
bool
forEach(_Container const &_container, Compound const &_compound, Inheritances::Callback _callback)
{
    auto it = _container.find(_compound);
    if (it != _container.end())
    {
        for (const auto &[compound, type] : it->second)
        {
            if (!_callback(compound, type))
            {
                return false;
            }
        }
    }

    return true;
}

bool
InheritancesImpl::forEachBaseType(Compound const &_derivative, Callback _callback) const
{
    return forEach(m_bases, _derivative, std::move(_callback));
}

bool
InheritancesImpl::forEachDerivedType(Compound const &_base, Callback _callback) const
{
    return forEach(m_derivatives, _base, std::move(_callback));
}

bool
InheritancesImpl::EdgeComparator::operator()(Edge const &_lhs, Edge const &_rhs) const
{
    if (_lhs.second == _rhs.second)
    {
        return _lhs.first.get().getName() < _rhs.first.get().getName();
    }

    return _lhs.second < _rhs.second;
}

} // namespace Storage
} // namespace Inhale
