#ifndef __INHALE_STORAGE_SRC_COMPOUNDS_FUNCTIONIMPL_HPP__
#define __INHALE_STORAGE_SRC_COMPOUNDS_FUNCTIONIMPL_HPP__

#include "inhale/storage/compounds/function.hpp"

#include "src/compounds/compound_base_impl.hpp"

#include <vector>

namespace Inhale {
namespace Storage {

class FunctionImpl final
    : public OptimizedCompoundBaseImpl<Function, CompoundType::Function>
{
public:

    using OptimizedCompoundBaseImpl::OptimizedCompoundBaseImpl;


    void setReturnType(std::string _type) final;

    std::string_view getReturnType() const noexcept final;


    std::size_t getParamsCount() const noexcept final;

    void addParam(std::string _param) final;

    bool forEachParam(ParamCallback _callback) const final;


    std::size_t getTemplateParamsCount() const noexcept final;

    void addTemplateParam(std::string _param) final;

    bool forEachTemplateParam(TemplateParamCallback _callback) const final;

private:

    std::string m_returnType;
    std::vector<std::string> m_params;
    std::vector<std::string> m_templateParams;
};

void
FunctionImpl::setReturnType(std::string _type)
{
    m_returnType = std::move(_type);
}

inline std::string_view
FunctionImpl::getReturnType() const noexcept
{
    return m_returnType;
}

inline std::size_t
FunctionImpl::getParamsCount() const noexcept
{
    return m_params.size();
}

inline void
FunctionImpl::addParam(std::string _param)
{
    m_params.push_back(std::move(_param));
}

inline bool
FunctionImpl::forEachParam(ParamCallback _callback) const
{
    for (const auto &param : m_params)
    {
        if (!_callback(param))
        {
            return false;
        }
    }

    return true;
}

inline std::size_t
FunctionImpl::getTemplateParamsCount() const noexcept
{
    return m_templateParams.size();
}

inline void
FunctionImpl::addTemplateParam(std::string _param)
{
    m_templateParams.push_back(std::move(_param));
}

inline bool
FunctionImpl::forEachTemplateParam(TemplateParamCallback _callback) const
{
    for (const auto &param : m_templateParams)
    {
        if (!_callback(param))
        {
            return false;
        }
    }

    return true;
}

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_SRC_COMPOUNDS_FUNCTIONIMPL_HPP__
