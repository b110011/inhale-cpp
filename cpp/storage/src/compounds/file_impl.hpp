#ifndef __INHALE_STORAGE_SRC_COMPOUNDS_FILEIMPL_HPP__
#define __INHALE_STORAGE_SRC_COMPOUNDS_FILEIMPL_HPP__

#include "inhale/storage/compounds/file.hpp"

#include "src/compounds/compound_base_impl.hpp"

namespace Inhale {
namespace Storage {

class FileImpl final
    : public OptimizedCompoundBaseImpl<File, CompoundType::File>
{
public:

    using OptimizedCompoundBaseImpl::OptimizedCompoundBaseImpl;


    void setLanguage(FileLanguage _language) final;

    FileLanguage getLanguage() const noexcept final;


    void setSourceLocation(std::string _path) final;

    std::string_view getSourceLocation() const noexcept final;

private:

    FileLanguage m_language;
    std::string m_location;
};

inline void
FileImpl::setLanguage(FileLanguage _language)
{
    m_language = std::move(_language);
}

inline FileLanguage
FileImpl::getLanguage() const noexcept
{
    return m_language;
}

inline void
FileImpl::setSourceLocation(std::string _path)
{
    m_location = std::move(_path);
}

inline std::string_view
FileImpl::getSourceLocation() const noexcept
{
    return m_location;
}

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_SRC_COMPOUNDS_FILEIMPL_HPP__
