#ifndef __INHALE_STORAGE_SRC_COMPOUNDS_DIRECTORYIMPL_HPP__
#define __INHALE_STORAGE_SRC_COMPOUNDS_DIRECTORYIMPL_HPP__

#include "inhale/storage/compounds/directory.hpp"

#include "src/compounds/compound_base_impl.hpp"

namespace Inhale {
namespace Storage {

class DirectoryImpl final
    : public OptimizedCompoundBaseImpl<Directory, CompoundType::Directory>
{
public:

    using OptimizedCompoundBaseImpl::OptimizedCompoundBaseImpl;

    void setPath(std::string _path) final;

    std::string_view getPath() const noexcept final;

private:

    std::string m_path;
};

inline void
DirectoryImpl::setPath(std::string _path)
{
    m_path = std::move(_path);
}

inline std::string_view
DirectoryImpl::getPath() const noexcept
{
    return m_path;
}

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_SRC_COMPOUNDS_DIRECTORYIMPL_HPP__
