#ifndef __INHALE_STORAGE_SRC_COMPOUNDS_COMPOUNDBASEIMPL_HPP__
#define __INHALE_STORAGE_SRC_COMPOUNDS_COMPOUNDBASEIMPL_HPP__

#include "inhale/storage/compound_type.hpp"

#include <string>
#include <string_view>

namespace Inhale {
namespace Storage {

template<typename _DerivedT>
class CompoundBaseImpl
    : public _DerivedT
{
public:

    CompoundBaseImpl(
        std::string _id,
        std::string _name,
        CompoundType _type
    ) noexcept
        : m_id{std::move(_id)}
        , m_name{std::move(_name)}
        , m_type{_type}
    {}

    ~CompoundBaseImpl() noexcept override = default;

    std::string_view getId() const noexcept final
    {
        return m_id;
    }

    std::string_view getName() const noexcept final
    {
        return m_name;
    }

    CompoundType getType() const noexcept final
    {
        return m_type;
    }

private:

    std::string m_id;
    std::string m_name;
    CompoundType m_type;
};

template<typename _DerivedT, CompoundType _type>
class OptimizedCompoundBaseImpl
    : public _DerivedT
{
public:

    OptimizedCompoundBaseImpl(std::string _id, std::string _name) noexcept
        : m_id{std::move(_id)}
        , m_name{std::move(_name)}
    {}

    ~OptimizedCompoundBaseImpl() noexcept override = default;

    std::string_view getId() const noexcept final
    {
        return m_id;
    }

    std::string_view getName() const noexcept final
    {
        return m_name;
    }

    CompoundType getType() const noexcept final
    {
        return _type;
    }

private:

    std::string m_id;
    std::string m_name;
};

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_SRC_COMPOUNDS_COMPOUNDBASEIMPL_HPP__
