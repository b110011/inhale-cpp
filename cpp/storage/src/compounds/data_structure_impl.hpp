#ifndef __INHALE_STORAGE_SRC_COMPOUNDS_DATASTRUCTUREIMPL_HPP__
#define __INHALE_STORAGE_SRC_COMPOUNDS_DATASTRUCTUREIMPL_HPP__

#include "inhale/storage/compounds/data_structure.hpp"

#include "src/compounds/compound_base_impl.hpp"

#include <vector>

namespace Inhale {
namespace Storage {

class DataStructureImpl final
    : public CompoundBaseImpl<DataStructure>
{
public:

    using CompoundBaseImpl::CompoundBaseImpl;


    std::size_t getTemplateParamsCount() const noexcept final;

    void addTemplateParam(TemplateParam _param) final;

    bool forEachTemplateParam(Callback _callback) const final;

private:

    std::vector<TemplateParam> m_params;
};

inline std::size_t
DataStructureImpl::getTemplateParamsCount() const noexcept
{
    return m_params.size();
}

inline void
DataStructureImpl::addTemplateParam(TemplateParam _param)
{
    m_params.push_back(std::move(_param));
}

inline bool
DataStructureImpl::forEachTemplateParam(Callback _callback) const
{
    for (const auto &param : m_params)
    {
        if (!_callback(param))
        {
            return false;
        }
    }

    return true;
}

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_SRC_COMPOUNDS_DATASTRUCTUREIMPL_HPP__
