#ifndef __INHALE_STORAGE_SRC_COMPOUNDS_COMPOUNDIMPL_HPP__
#define __INHALE_STORAGE_SRC_COMPOUNDS_COMPOUNDIMPL_HPP__

#include "inhale/storage/compound.hpp"

#include "src/compounds/compound_base_impl.hpp"

namespace Inhale {
namespace Storage {

class CompoundImpl
    : public CompoundBaseImpl<Compound>
{
public:

    using CompoundBaseImpl::CompoundBaseImpl;
};

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_SRC_COMPOUNDS_COMPOUNDIMPL_HPP__
