#ifndef __INHALE_STORAGE_SRC_COMPOUNDS_PAGEIMPL_HPP__
#define __INHALE_STORAGE_SRC_COMPOUNDS_PAGEIMPL_HPP__

#include "inhale/storage/compounds/page.hpp"

#include "src/compounds/compound_base_impl.hpp"

namespace Inhale {
namespace Storage {

class PageImpl final
    : public OptimizedCompoundBaseImpl<Page, CompoundType::Page>
{
public:

    using OptimizedCompoundBaseImpl::OptimizedCompoundBaseImpl;


    void setSourceLocation(std::string _path) final;

    std::string_view getSourceLocation() const noexcept final;


    void setTitle(std::string _language) final;

    std::string_view getTitle() const noexcept final;

private:

    std::string m_location;
    std::string m_title;
};

inline void
PageImpl::setSourceLocation(std::string _path)
{
    m_location = std::move(_path);
}

inline std::string_view
PageImpl::getSourceLocation() const noexcept
{
    return m_location;
}

inline void
PageImpl::setTitle(std::string _title)
{
    m_title = std::move(_title);
}

inline std::string_view
PageImpl::getTitle() const noexcept
{
    return m_title;
}

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_SRC_COMPOUNDS_PAGEIMPL_HPP__
