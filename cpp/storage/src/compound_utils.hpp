#ifndef __INHALE_STORAGE_SRC_COMPOUNDUTILS_HPP__
#define __INHALE_STORAGE_SRC_COMPOUNDUTILS_HPP__

#include "inhale/storage/compound.hpp"

#include <magic_enum.hpp>

#include <array>
#include <set>
#include <string_view>

namespace Inhale {
namespace Storage {

struct CompoundHasher final
{
    using hash_type = std::hash<std::string_view>;
    using is_transparent = void;

    std::size_t operator()(std::string_view _data) const noexcept;

    std::size_t operator()(Compound const &_compound) const;
};

struct CompoundSimpleComparator final
{
    using hash_type = std::hash<std::string_view>;
    using is_transparent = void;

    bool operator()(std::string_view _data, Compound const &_compound) const;

    bool operator()(Compound const &_lhs, Compound const &_rhs) const;
};

struct CompoundComparator final
{
    bool operator()(Compound const &_lhs, Compound const &_rhs) const;
};

template<typename _CompoundRefT>
using CompoundsByType = std::array<
    std::set<_CompoundRefT, CompoundComparator>,
    magic_enum::enum_count<CompoundType>()
>;

inline constexpr std::size_t
asIndex(CompoundType _type) noexcept
{
    return static_cast<std::size_t>(_type);
}

} // namespace Storage
} // namespace Inhale

#endif // __INHALE_STORAGE_SRC_COMPOUNDUTILS_HPP__
