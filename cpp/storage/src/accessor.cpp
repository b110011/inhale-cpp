#include "inhale/storage/accessor.hpp"

#include "src/context_impl.hpp"

namespace Inhale {
namespace Storage {

std::unique_ptr<Context>
createContext()
{
    return std::make_unique<ContextImpl>();
}

} // namespace Storage
} // namespace Inhale
