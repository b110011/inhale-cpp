#include "src/context_impl.hpp"

#include "src/compounds/compound_impl.hpp"
#include "src/compounds/data_structure_impl.hpp"
#include "src/compounds/directory_impl.hpp"
#include "src/compounds/file_impl.hpp"
#include "src/compounds/function_impl.hpp"
#include "src/compounds/page_impl.hpp"
#include "src/inheritances_impl.hpp"
#include "src/relations_impl.hpp"

#include "inhale/common/format/enums.hpp"
#include "inhale/logging.hpp"

#include <magic_enum.hpp>

#include <cassert>

namespace Inhale {
namespace Storage {

ContextImpl::ContextImpl()
    : m_pFileRelations{std::make_unique<RelationsImpl>()}
    , m_pInheritances{std::make_unique<InheritancesImpl>()}
    , m_pRelations{std::make_unique<RelationsImpl>()}
{}

ContextImpl::~ContextImpl() = default;

// void
// ContextImpl::setCurrentProject(std::string _project)
// {
//     m_projects.emplace_back(_project);
// }

// std::string_view
// ContextImpl::getCurrentProject() const
// {
//     assert(!m_projects.empty());
//     return m_projects.back();
// }

// template<typename _CompoundImplT, typename... _ArgsT>
// std::unique_ptr<_CompoundImplT>
// ContextImpl::makeCompound(
//     std::string _id,
//     std::string _name,
//     _ArgsT &&... _args
// )
// {
//     return std::make_unique<_CompoundImplT>(
//         getCurrentProject(),
//         std::move(_id),
//         std::move(_name),
//         _args...
//     );
// }

Compound &
ContextImpl::createCompound(std::string _id, std::string _name, CompoundType _type)
{
    std::unique_ptr<Compound> pCompound;

    switch (_type)
    {
        case CompoundType::Class:
        case CompoundType::Struct:
            pCompound = std::make_unique<DataStructureImpl>(
                std::move(_id),
                std::move(_name),
                _type
            );
            break;

        case CompoundType::Directory:
            pCompound = std::make_unique<DirectoryImpl>(std::move(_id), std::move(_name));
            break;

        case CompoundType::File:
            pCompound = std::make_unique<FileImpl>(std::move(_id), std::move(_name));
            break;

        case CompoundType::Function:
            pCompound = std::make_unique<FunctionImpl>(std::move(_id), std::move(_name));
            break;

        case CompoundType::Page:
            pCompound = std::make_unique<PageImpl>(std::move(_id), std::move(_name));
            break;

        default:
            pCompound = std::make_unique<CompoundImpl>(
                std::move(_id),
                std::move(_name),
                _type
            );
            break;
    }

    const auto newCompoundId = pCompound->getId();
    const auto[it, isInserted] = m_compounds.emplace(newCompoundId, std::move(pCompound));
    if (isInserted)
    {
        m_compoundsByType[asIndex(_type)].emplace(*it->second);
    }

    return *it->second;
}

// void
// ContextImpl::reloc(Compound const &_compound)
// {
//     std::string_view loc;
//     switch (_compound.getType())
//     {
//         case CompoundType::File:
//             loc = static_cast<File const &>(_compound).getSourceLocation();
//             break;

//         case CompoundType::Page:
//             loc = static_cast<Page const &>(_compound).getSourceLocation();
//             break;

//         default:
//             INHALE_WARN(
//                 "'reloc' method got unsupported compound type: {}.",
//                 _compound.getType()
//             );
//             return;
//     }

//     if (auto it = m_compounds.find(_compound.getId()); it != m_compounds.end())
//     {
//         auto pCompound = std::move(it->second);
//         m_compounds.erase(it);
//         m_compounds.emplace(loc, std::move(pCompound));
//         return;
//     }

//     INHALE_ERROR("'reloc' method got unexpected compound with '{}' id.", _compound);
//     throw std::out_of_range{"Failed to find compound by id!"};
// }

std::size_t
ContextImpl::getCompoundsCountByType(CompoundType _type) const noexcept
{
    return m_compoundsByType[asIndex(_type)].size();
}

template<typename _ContainerT>
decltype(auto)
findCompoundById(_ContainerT &_container, std::string_view _id)
{
    if (auto it = _container.find(_id); it != _container.end())
    {
        return *it->second;
    }

    INHALE_ERROR("Failed to find compound with '{}' id!", _id);
    throw std::out_of_range{"Failed to find compound by id!"};
}

Compound const &
ContextImpl::getCompoundById(std::string_view _id) const
{
    return findCompoundById(m_compounds, _id);
}

Compound &
ContextImpl::takeCompoundById(std::string_view _id)
{
    return findCompoundById(m_compounds, _id);
}

template<typename _ContainerT, typename _CompoundT>
bool
forEach(_ContainerT & _container, CompoundType _type, Context::Callback<_CompoundT> _callback)
{
    for (auto &compound : _container[asIndex(_type)])
    {
        if (!_callback(compound))
        {
            return false;
        }
    }

    return true;
}

bool
ContextImpl::forEachCompoundByType(CompoundType _type, Callback<Compound const> _callback) const
{
    return forEach(m_compoundsByType, _type, std::move(_callback));
}

bool
ContextImpl::forEachCompoundByType(CompoundType _type, Callback<Compound> _callback)
{
    return forEach(m_compoundsByType, _type, std::move(_callback));
}

Relations const &
ContextImpl::getFileRelations() const noexcept
{
    assert(m_pFileRelations);
    return *m_pFileRelations;
}

Relations &
ContextImpl::takeFileRelations() noexcept
{
    assert(m_pFileRelations);
    return *m_pFileRelations;
}

Inheritances const &
ContextImpl::getInheritances() const noexcept
{
    assert(m_pInheritances);
    return *m_pInheritances;
}

Inheritances &
ContextImpl::takeInheritances() noexcept
{
    assert(m_pInheritances);
    return *m_pInheritances;
}

Relations const &
ContextImpl::getRelations() const noexcept
{
    assert(m_pRelations);
    return *m_pRelations;
}

Relations &
ContextImpl::takeRelations() noexcept
{
    assert(m_pRelations);
    return *m_pRelations;
}

} // namespace Storage
} // namespace Inhale
