#include "src/relations_impl.hpp"

#include "inhale/logging.hpp"

namespace Inhale {
namespace Storage {

void
RelationsImpl::add(Compound const &_parent, Compound const &_child)
{
    if (&_parent == &_child)
    {
        INHALE_ERROR("Tried to create a relation loop for {}!", _parent);
        throw std::runtime_error{"Detected a loop!"};
    }

    if (m_children[_parent][asIndex(_child.getType())].emplace(_child).second)
    {
        m_parents[_child][asIndex(_parent.getType())].emplace(_parent);
    }

    // TODO(b110011): Throw an exception if edge already exists?
}

template<typename _Container>
std::size_t
getCount(_Container const &_container, Compound const &_compound, CompoundType _type)
{
    auto it = _container.find(_compound);
    if (it != _container.end())
    {
        return it->second[asIndex(_type)].size();
    }

    return 0;
}

std::size_t
RelationsImpl::getChildrenCountByType(
    Compound const &_compound,
    CompoundType _type
) const noexcept
{
    return getCount(m_children, _compound, _type);
}

std::size_t
RelationsImpl::getParentsCountByType(
    Compound const &_compound,
    CompoundType _type
) const noexcept
{
    return getCount(m_parents, _compound, _type);
}

template<typename _ContainerT>
bool
forEach(
    _ContainerT const &_compounds,
    Compound const &_compound,
    CompoundType _type,
    Relations::Callback _callback
)
{
    auto it = _compounds.find(_compound);
    if (it != _compounds.end())
    {
        for (const auto &compound : it->second[asIndex(_type)])
        {
            if (!_callback(compound))
            {
                return false;
            }
        }
    }

    return true;
}

bool
RelationsImpl::forEachChildByType(
    Compound const &_compound,
    CompoundType _type,
    Callback _callback
) const
{
    return forEach(m_children, _compound, _type, std::move(_callback));
}

bool
RelationsImpl::forEachParentByType(
    Compound const &_compound,
    CompoundType _type,
    Callback _callback
) const
{
    return forEach(m_parents, _compound, _type, std::move(_callback));
}

} // namespace Storage
} // namespace Inhale
