#ifndef __INHALE_RENDER_SRC_FLAT_DIRECTORIESCONTENTRENDER_HPP__
#define __INHALE_RENDER_SRC_FLAT_DIRECTORIESCONTENTRENDER_HPP__

#include "inhale/render_fwd.hpp"
#include "inhale/storage_fwd.hpp"

#include <iosfwd>

namespace Inhale {
namespace Render {

void
renderDirectory(
    Storage::Context const &_ctx,
    Storage::Directory const &_dir,
    Config const &_config,
    std::ostream &_o
);

void
renderDirectories(Storage::Context const &_ctx, Config const &_config);

} // namespace Render
} // namespace Inhale

#endif // __INHALE_RENDER_SRC_FLAT_DIRECTORIESCONTENTRENDER_HPP__
