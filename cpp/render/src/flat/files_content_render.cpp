#include "src/flat/files_content_render.hpp"

#include "inhale/l10n/ids.hpp"
#include "inhale/render_config.hpp"
#include "inhale/storage/compounds/directory.hpp"
#include "inhale/storage/context.hpp"
#include "inhale/storage/relations.hpp"

#include "ih/l10n/compound_type.hpp"
#include "ih/rst/directives/doc.hpp"
#include "ih/rst/directives/orphan.hpp"
#include "ih/rst/formatting/heading.hpp"
#include "ih/rst/formatting/italic.hpp"
#include "ih/rst/formatting/list.hpp"
#include "ih/rst/formatting/monospace.hpp"
#include "ih/file.hpp"

#include <format>

namespace Inhale {
namespace Render {

void
renderDirectory(
    Storage::Context const &_ctx,
    Storage::Directory const &_dir,
    Config const &_config,
    std::ostream &_o
)
{
    std::ostream_iterator<char> out{_o};

    orphan(out);
    out = '\n';

    h1(_o, "{} {}", _dir.getType(), _dir.getName());
    out = '\n';

    auto const &relations = _ctx.getRelations();
    relations.forEachParentByType(
        _dir,
        Storage::CompoundType::Directory,
        [&_config, out](auto const & _parentDir) mutable
        {
            std::format_to(out, ".. |back_symbol| unicode:: U+021B0 .. UPWARDS ARROW WITH TIP LEFTWARDS\n");
            out = '\n';
            std::format_to(
                out,
                "|back_symbol| {} ({})\n",
                doc(_(ParentDirectory), _parentDir, _config),
                monospace(_parentDir.getName())
            );
            out = '\n';

            return true;
        }
    );

    std::format_to(out, "{}: {}\n", italic(_(DirectoryPath)), monospace(_dir.getPath()));
    out = '\n';

    if (relations.getChildrenCountByType(_dir, Storage::CompoundType::Directory) != 0)
    {
        h2(_o, "{}", _(Subdirectories));
        out = '\n';
    }

    relations.forEachChildByType(
        _dir,
        Storage::CompoundType::Directory,
        [&_config, out](auto const & _childDir)
        {
            ul(out, "{}", doc(_childDir, _config));
            return true;
        }
    );

    if (relations.getChildrenCountByType(_dir, Storage::CompoundType::File) != 0)
    {
        h2(_o, "{}", _(Files));
        out = '\n';
    }

    relations.forEachChildByType(
        _dir,
        Storage::CompoundType::File,
        [&_config, out](auto const & _childFile)
        {
            ul(out, "{}", doc(_childFile, _config));
            return true;
        }
    );
}

void
renderDirectories(Storage::Context const &_ctx, Config const &_config)
{
    _ctx.forEachCompoundByType(
        Storage::CompoundType::Directory,
        [&_ctx, &_config](Storage::Compound const &_compound)
        {
            auto file = openFile(_compound, _config);
            renderDirectory(
                _ctx,
                static_cast<Storage::Directory const &>(_compound),
                _config,
                file
            );
            return true;
        }
    );
}

} // namespace Render
} // namespace Inhale
