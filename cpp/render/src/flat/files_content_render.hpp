#ifndef __INHALE_RENDER_SRC_FLAT_FILESCONTENTRENDER_HPP__
#define __INHALE_RENDER_SRC_FLAT_FILESCONTENTRENDER_HPP__

#include "inhale/render_fwd.hpp"
#include "inhale/storage_fwd.hpp"

#include <iosfwd>

namespace Inhale {
namespace Render {

void
renderFile(
    Storage::Context const &_ctx,
    Storage::File const &_file,
    Config const &_config,
    std::ostream &_o
);

void
renderFiles(Storage::Context const &_ctx, Config const &_config);

} // namespace Render
} // namespace Inhale

#endif // __INHALE_RENDER_SRC_FLAT_FILESCONTENTRENDER_HPP__
