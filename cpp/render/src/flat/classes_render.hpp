#ifndef __INHALE_RENDER_SRC_FLAT_CLASSESRENDER_HPP__
#define __INHALE_RENDER_SRC_FLAT_CLASSESRENDER_HPP__

#include "inhale/render_fwd.hpp"
#include "inhale/storage_fwd.hpp"

#include <iosfwd>

namespace Inhale {
namespace Render {

void
renderClass(
    Storage::Context const &_ctx,
    Storage::DataStructure const &_ds,
    Config const &_config,
    std::ostream &_o
);

void
renderClasses(Storage::Context const &_ctx, Config const &_config);

} // namespace Render
} // namespace Inhale

#endif // __INHALE_RENDER_SRC_FLAT_CLASSESRENDER_HPP__
