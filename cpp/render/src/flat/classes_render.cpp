#include "src/classes_render.hpp"

#include "inhale/common/format/enums.hpp"
#include "inhale/render_config.hpp"
#include "inhale/storage/compounds/data_structure.hpp"
#include "inhale/storage/context.hpp"
#include "inhale/storage/inheritances.hpp"

#include "ih/rst/formatting/heading.hpp"
#include "ih/file.hpp"

#include <magic_enum.hpp>

#include <format>
#include <string_view>

namespace Inhale {
namespace Render {
namespace {

[[nodiscard]]
constexpr std::string_view
toString(Storage::InheritanceType _type) noexcept
{
    static_assert(magic_enum::enum_count<Storage::InheritanceType>() == 6);
    switch(_type)
    {
        case Storage::InheritanceType::Public:
            return "public";

        case Storage::InheritanceType::VirtualPublic:
            return "virtual public";

        case Storage::InheritanceType::Protected:
            return "protected";

        case Storage::InheritanceType::VirtualProtected:
            return "virtual protected";

        case Storage::InheritanceType::Private:
            return "private";

        case Storage::InheritanceType::VirtualPrivate:
            return "virtual private";
    }
}

} // namespace

void
renderClass(
    Storage::Context const &_ctx,
    Storage::DataStructure const &_ds,
    Config const &_config,
    std::ostream &_o
)
{
    h1(
        _o,
        "{}{} {}",
        _ds.getTemplateParamsCount() ? "" : "Template ",
        _ds.getType(),
        _ds.getName()
    );

    // std::format_to(_o, - Defined in :ref:`file_data_model_message_accessor.hpp`);

    auto const &inheritance = _ctx.getInheritances();
    auto const baseTypesCount = inheritance.getBaseTypesCount();
    auto const derivedTypesCount = inheritance.getDerivedTypesCount();

    if ((baseTypesCount != 0) && (derivedTypesCount != 0))
    {
        h2(_o, "Inheritance Relationships");
    }

    if (baseTypesCount == 1)
    {
        h3(_o, "Base Type");
    }
    else if (baseTypesCount > 1)
    {
        h3(_o, "Base Types");
    }

    inheritance.forEachBaseType(
        _ds
        [&_o](auto const & _baseType, auto _inheritanceType)
        {
            std::format_to(_o, "- {}\n", );  // TODO
        }
    );

    if (derivedTypesCount == 1)
    {
        h3(_o, "Derived Type");
    }
    else if (derivedTypesCount > 1)
    {
        h3(_o, "Derived Types");
    }

    inheritance.forEachDerivedType(
        _ds
        [&_o](auto const & _derivedType, auto _inheritanceType)
        {
            std::format_to(_o, "- {}", );  // TODO
        }
    );

    // TODO: Template parameter listing

    h2(_o, "{} Documentation\n", _ds.getType());

    std::format_to(_o, ".. doxygenclass:: {}\n", "");  // TODO: Fts::DataModel::Message::ReadWriteMessage
    std::format_to(_o, "   :project: {}\n", _config.project);

    auto const itDoxygenClassArgs = _config.doxygenDirectivesArgs.find("doxygenclass");
    if (itDoxygenClassArgs != _config.doxygenDirectivesArgs.end())
    {
        for (auto const &arg : itDoxygenClassArgs->second)
        {
            std::format_to(_o, "   :{}:\n", arg);
        }
    }
    else
    {
        for (auto const &arg : {"members", "protected-members", "undoc-members"})
        {
            std::format_to(_o, "   :{}:\n", arg);
        }
    }
}

void
renderClasses(Storage::Context const &_ctx, Config const &_config)
{
    auto callback = [&_ctx, &_config](Storage::Compound const &_ds)
    {
        renderClass(_ctx, _ds, _config, openFile(_ds, _config));
        return true;
    };

    _ctx.forEachCompoundByType(Storage::CompoundType::Class, callback);
    _ctx.forEachCompoundByType(Storage::CompoundType::Struct, callback);
}

} // namespace Render
} // namespace Inhale
