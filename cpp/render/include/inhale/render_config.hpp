#ifndef __INHALE_RENDERCONFIG_HPP__
#define __INHALE_RENDERCONFIG_HPP__

#include <cstdint>
#include <filesystem>
#include <string>
#include <vector>
#include <unordered_map>

namespace Inhale {
namespace Render {

struct Config
{
    std::string project;
    std::string output;

    bool copySource;  // Sphinx html_copy_source
    bool generateClassHierarchyPage;
    bool generateFilesIndexPage;
    bool useShortFileNames;

    std::unordered_map<std::string, std::vector<std::string>> doxygenDirectivesArgs;  // Exhale customSpecificationsMapping
};

} // namespace Render
} // namespace Inhale

#endif // __INHALE_RENDERCONFIG_HPP__
