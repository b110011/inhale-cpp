#ifndef __INHALE_RENDER_RENDER_HPP__
#define __INHALE_RENDER_RENDER_HPP__

#include "inhale/render_fwd.hpp"
#include "inhale/storage_fwd.hpp"

namespace Inhale {
namespace Render {

void
render(Storage::Context const &_ctx, Config const &_config);

} // namespace Render
} // namespace Inhale

#endif // __INHALE_RENDER_RENDER_HPP__
