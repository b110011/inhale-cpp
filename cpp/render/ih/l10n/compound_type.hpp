#ifndef __INHALE_RENDER_IH_L10N_COMPOUNDTYPE_HPP__
#define __INHALE_RENDER_IH_L10N_COMPOUNDTYPE_HPP__

#include "inhale/l10n/ids.hpp"
#include "inhale/storage/compound_type.hpp"

#include <magic_enum.hpp>

#include <format>

namespace {

[[nodiscard]]
constexpr Inhale::L10n::Id
convert(Inhale::Storage::CompoundType _type)
{
    static_assert(magic_enum::enum_count<Inhale::Storage::CompoundType>() == 15);
    switch (_type)
    {
        case Inhale::Storage::CompoundType::Class:
            return Inhale::L10n::Id::Class;

        case Inhale::Storage::CompoundType::Concept:
            return Inhale::L10n::Id::Concept;

        case Inhale::Storage::CompoundType::Define:
            return Inhale::L10n::Id::Define;

        case Inhale::Storage::CompoundType::Directory:
            return Inhale::L10n::Id::Directory;

        case Inhale::Storage::CompoundType::Enum:
            return Inhale::L10n::Id::Enum;

        case Inhale::Storage::CompoundType::File:
            return Inhale::L10n::Id::File;

        case Inhale::Storage::CompoundType::Function:
            return Inhale::L10n::Id::Function;

        case Inhale::Storage::CompoundType::Group:
            return Inhale::L10n::Id::Group;

        case Inhale::Storage::CompoundType::Namespace:
            return Inhale::L10n::Id::Namespace;

        case Inhale::Storage::CompoundType::Page:
            return Inhale::L10n::Id::Page;

        case Inhale::Storage::CompoundType::Struct:
            return Inhale::L10n::Id::Struct;

        case Inhale::Storage::CompoundType::Typedef:
            return Inhale::L10n::Id::Typedef;

        case Inhale::Storage::CompoundType::Union:
            return Inhale::L10n::Id::Union;

        case Inhale::Storage::CompoundType::Variable:
            return Inhale::L10n::Id::Variable;

        default:
            throw std::logic_error{"Unexpected compound type!"};
    }
}

} // namespace

template<>
struct std::formatter<Inhale::Storage::CompoundType> final
    : std::formatter<std::string_view>
{
    constexpr auto parse(auto &_ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(Inhale::Storage::CompoundType _data, std::format_context &_ctx) const
    {
        auto const value = Inhale::L10n::getString(convert(_data));
        return std::formatter<std::string_view>::format(value, _ctx);
    }
};

#endif // __INHALE_RENDER_IH_L10N_COMPOUNDTYPE_HPP__
