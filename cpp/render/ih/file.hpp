#ifndef __INHALE_RENDER_IH_FILE_HPP__
#define __INHALE_RENDER_IH_FILE_HPP__

#include "inhale/logging.hpp"

#include "ih/file_name.hpp"

#include <format>
#include <fstream>

namespace Inhale {
namespace Render {

[[nodiscard]]
std::ofstream
openFile(Storage::Compound const &_compound, Config const &_config)
{
    std::string filePath = std::format(
        "{}/{}.rst",
        _config.output,
        makeFileName(_compound, _config)
    );

    std::ofstream ofs{filePath, std::ios::trunc};
    if (!ofs)
    {
        throw std::runtime_error{"Failed to open a file!"};
    }

    return ofs;
}

} // namespace Render
} // namespace Inhale

#endif // __INHALE_RENDER_IH_FILE_HPP__
