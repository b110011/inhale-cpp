#ifndef __INHALE_RENDER_IH_RST_FORMATTING_ITALIC_HPP__
#define __INHALE_RENDER_IH_RST_FORMATTING_ITALIC_HPP__

#include <format>
#include <string_view>

namespace Inhale {
namespace Render {

struct Italic  // or emphasis
{
    std::string_view content;
};

[[nodiscard]]
inline Italic
italic(std::string_view _content) noexcept
{
    return {_content};
}

} // namespace Render
} // namespace Inhale

template<>
struct std::formatter<Inhale::Render::Italic> final
{
    constexpr auto parse(auto &_ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(Inhale::Render::Italic _data, std::format_context &_ctx) const
    {
        return std::format_to(_ctx.out(), "*{}*", _data.content);
    }
};

#endif // __INHALE_RENDER_IH_RST_FORMATTING_ITALIC_HPP__
