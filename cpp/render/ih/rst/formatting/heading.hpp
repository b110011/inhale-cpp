#ifndef __INHALE_RENDER_IH_RST_FORMATTING_HEADING_HPP__
#define __INHALE_RENDER_IH_RST_FORMATTING_HEADING_HPP__

#include <format>
#include <iterator>
#include <ostream>

namespace Inhale {
namespace Render {

template<char _headingSymbol, typename... _ArgsT>
void
heading(std::ostream &_o, std::format_string<_ArgsT...> _fmt, _ArgsT&&... _args)
{
    std::ostream_iterator<char> out{_o};
    auto const oldPos = _o.tellp();

    std::format_to(out, _fmt, std::forward<_ArgsT>(_args)...);
    out = '\n';

    auto const headingLength = _o.tellp() - oldPos;
    auto heading = std::views::repeat(_headingSymbol, headingLength);

    std::ranges::copy(heading, out);
    out = '\n';
}

#define INHALE_HEADING(_FNNAME, _HEADING_SYMBOL)                                        \
    template<typename... _ArgsT>                                                        \
    void                                                                                \
    _FNNAME(std::ostream &_o, std::format_string<_ArgsT...> _fmt, _ArgsT&&... _args)    \
    {                                                                                   \
        heading<_HEADING_SYMBOL>(_o, _fmt, std::forward<_ArgsT>(_args)...);             \
    }                                                                                   \

INHALE_HEADING(h1, '#');
INHALE_HEADING(h2, '*');
INHALE_HEADING(h3, '=');
INHALE_HEADING(h4, '-');
INHALE_HEADING(h5, '^');
INHALE_HEADING(h6, '"');

#undef INHALE_HEADING

} // namespace Render
} // namespace Inhale

#endif // __INHALE_RENDER_IH_RST_FORMATTING_HEADING_HPP__
