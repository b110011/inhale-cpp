#ifndef __INHALE_RENDER_IH_RST_FORMATTING_BOLD_HPP__
#define __INHALE_RENDER_IH_RST_FORMATTING_BOLD_HPP__

#include <format>
#include <string_view>

namespace Inhale {
namespace Render {

struct Bold  // strong emphasis
{
    std::string_view content;
};

[[nodiscard]]
inline Bold
bold(std::string_view _content) noexcept
{
    return {_content};
}

} // namespace Render
} // namespace Inhale

template<>
struct std::formatter<Inhale::Render::Bold> final
{
    constexpr auto parse(auto &_ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(Inhale::Render::Bold _data, std::format_context &_ctx) const
    {
        return std::format_to(_ctx.out(), "**{}**", _data.content);
    }
};

#endif // __INHALE_RENDER_IH_RST_FORMATTING_BOLD_HPP__
