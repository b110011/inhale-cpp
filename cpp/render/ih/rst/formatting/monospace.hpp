#ifndef __INHALE_RENDER_IH_RST_FORMATTING_MONOSPACE_HPP__
#define __INHALE_RENDER_IH_RST_FORMATTING_MONOSPACE_HPP__

#include <format>
#include <string_view>

namespace Inhale {
namespace Render {

struct Monospace  // or code
{
    std::string_view content;
};

[[nodiscard]]
inline Monospace
monospace(std::string_view _content) noexcept
{
    return {_content};
}

} // namespace Render
} // namespace Inhale

template<>
struct std::formatter<Inhale::Render::Monospace> final
{
    constexpr auto parse(auto &_ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(Inhale::Render::Monospace _data, std::format_context &_ctx) const
    {
        return std::format_to(_ctx.out(), "``{}``", _data.content);
    }
};

#endif // __INHALE_RENDER_IH_RST_FORMATTING_MONOSPACE_HPP__
