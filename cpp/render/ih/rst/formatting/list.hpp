#ifndef __INHALE_RENDER_IH_RST_FORMATTING_LIST_HPP__
#define __INHALE_RENDER_IH_RST_FORMATTING_LIST_HPP__

#include <format>
#include <iterator>
#include <ranges>

namespace Inhale {
namespace Render {

template<std::output_iterator<char> _OutputItT, typename... _ArgsT>
void
ul(_OutputItT _itOut, std::format_string<_ArgsT...> _fmt, _ArgsT&&... _args)
{
    std::ranges::copy("- ", _itOut);
    std::format_to(_itOut, _fmt, std::forward<_ArgsT>(_args)...);

    _itOut = '\n';
}

} // namespace Render
} // namespace Inhale

#endif // __INHALE_RENDER_IH_RST_FORMATTING_LIST_HPP__
