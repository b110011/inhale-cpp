#ifndef __INHALE_RENDER_IH_RST_DIRECTIVES_TOC_HPP__
#define __INHALE_RENDER_IH_RST_DIRECTIVES_TOC_HPP__

#include <format>
#include <string_view>

namespace Inhale {
namespace Render {

struct Toc
{
    std::string_view caption;
};

struct TocEntry
{
    std::string_view document_path;
};

} // namespace Render
} // namespace Inhale

template<>
struct std::formatter<Inhale::Render::Toc> final
{
    constexpr auto parse(auto& _ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(Inhale::Render::Toc _data, std::format_context &_ctx) const
    {
        std::format_to(_ctx.out(), ".. toctree::\n");
        if (!_data.caption.empty())
        {
            std::format_to(_ctx.out(), "   :caption: {}\n", _data.caption);
        }
        std::format_to(_ctx.out(), "   :maxdepth: 1\n");
        std::format_to(_ctx.out(), '\n');

        return _ctx.out();
    }
};

template<>
struct std::formatter<Inhale::Render::TocEntry> final
{
    constexpr auto parse(auto& _ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(Inhale::Render::TocEntry _data, std::format_context &_ctx) const
    {
        return std::format_to(_ctx.out(), "   {}\n", _data.document_path);
    }
};

#endif // __INHALE_RENDER_IH_RST_DIRECTIVES_TOC_HPP__
