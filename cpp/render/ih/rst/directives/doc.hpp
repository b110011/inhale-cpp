#ifndef __INHALE_RENDER_IH_RST_DIRECTIVES_DOC_HPP__
#define __INHALE_RENDER_IH_RST_DIRECTIVES_DOC_HPP__

#include "ih/file_name.hpp"

#include <format>
#include <string_view>

namespace Inhale {
namespace Render {

struct DocTag1 {};
struct DocTag2 {};
struct DocTag3 {};

template<typename _ContentT>
struct Doc;

template<>
struct Doc<DocTag1>
{
    std::string_view content;
};

template<>
struct Doc<DocTag2>
{
    Storage::Compound const &compound;
    Config const &config;
};


template<>
struct Doc<DocTag3>
{
    std::string_view title;
    Storage::Compound const &compound;
    Config const &config;
};

[[nodiscard]]
inline Doc<DocTag1>
doc(std::string_view _content) noexcept
{
    return {_content};
}

[[nodiscard]]
inline Doc<DocTag2>
doc(Storage::Compound const &_compound, Config const &_config) noexcept
{
    return {_compound, _config};
}

[[nodiscard]]
inline Doc<DocTag3>
doc(std::string_view _title, Storage::Compound const &_compound, Config const &_config) noexcept
{
    return {_title, _compound, _config};
}

} // namespace Render
} // namespace Inhale

template<>
struct std::formatter<Inhale::Render::Doc<Inhale::Render::DocTag1>> final
{
    constexpr auto parse(auto &_ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(auto const &_data, auto &_ctx) const
    {
        return std::format_to(_ctx.out(), ":doc:`{}`", _data.content);
    }
};

template<>
struct std::formatter<Inhale::Render::Doc<Inhale::Render::DocTag2>> final
{
    constexpr auto parse(auto &_ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(auto const &_data, auto &_ctx) const
    {
        return std::format_to(
            _ctx.out(),
            ":doc:`{}`",
            makeFileName(_data.compound, _data.config)
        );
    }
};

template<>
struct std::formatter<Inhale::Render::Doc<Inhale::Render::DocTag3>> final
{
    constexpr auto parse(auto &_ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(auto const &_data, auto &_ctx) const
    {
        return std::format_to(
            _ctx.out(),
            ":doc:`{} <{}>`",
            _data.title,
            makeFileName(_data.compound, _data.config)
        );
    }
};

#endif // __INHALE_RENDER_IH_RST_DIRECTIVES_DOC_HPP__
