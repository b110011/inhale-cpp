#ifndef __INHALE_RENDER_IH_RST_DIRECTIVES_REF_HPP__
#define __INHALE_RENDER_IH_RST_DIRECTIVES_REF_HPP__

#include "inhale/storage/compound.hpp"

#include <format>
#include <ranges>

namespace Inhale {
namespace Render {

struct ReferenceTarget
{
    Storage::Compound const &compound;
};

struct Reference
{
    Storage::Compound const &compound;
    std::string_view name = {};
};

} // namespace Render
} // namespace Inhale

namespace {

[[nodiscard]]
auto
transformFileId(std::string_view _id) noexcept
{
    return std::views::transform(
        _id,
        [](char _ch) noexcept
        {
            switch (_ch)
            {
                [[unlikely]] case ' ':
                [[unlikely]] case '.':
                [[unlikely]] case '/':
                    return '_';

                [[likely]] default:
                    return _ch;
            }
        }
    );
}

} // namespace

template<>
struct std::formatter<Inhale::Render::ReferenceTarget> final
{
    constexpr auto parse(auto &_ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(Inhale::Render::ReferenceTarget const &_data, std::format_context &_ctx) const
    {
        auto const id = _data.compound.getId();
        switch(_data.compound.getType())
        {
            case Inhale::Storage::CompoundType::File:
            case Inhale::Storage::CompoundType::Page:
                return bake(_ctx.out(), transformFileId(id));

            default:
                return bake(_ctx.out(), id);
        };
    }

    template<typename _T>
    static auto bake(_T &&_id, std::format_context &_ctx)
    {
        return std::format_to(_ctx.out(), ".. _{}:", id);
    }
};

template<>
struct std::formatter<Inhale::Render::Reference> final
{
    constexpr auto parse(auto &_ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(Inhale::Render::Reference const &_data, std::format_context &_ctx) const
    {
        auto const id = data.compound.getId();
        switch(_data.compound.getType())
        {
            case Inhale::Storage::CompoundType::File:
            case Inhale::Storage::CompoundType::Page:
                return bake(_ctx.out(), transformFileId(id), _data.name);

            default:
                return bake(_ctx.out(), id, _data.name);
        };
    }

    template<typename _T>
    static auto bake(_T &&_id, std::string_view _name, std::format_context &_ctx)
    {
        if (_name.size())
        {
            return std::format_to(_ctx.out(), ":ref:`{} <{}>`", _name, _id);
        }
        return std::format_to(_ctx.out(), ":ref:`{}`", _id);
    }
};

#endif // __INHALE_RENDER_IH_RST_DIRECTIVES_REF_HPP__
