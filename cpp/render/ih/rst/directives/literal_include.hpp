#ifndef __INHALE_RENDER_IH_RST_DIRECTIVES_LITERALINCLUDE_HPP__
#define __INHALE_RENDER_IH_RST_DIRECTIVES_LITERALINCLUDE_HPP__

#include "inhale/storage/compounds/file.hpp"

#include <magic_enum.hpp>

#include <format>
#include <string_view>

namespace Inhale {
namespace Render {

struct LiteralInclude
{
    Storage::File const &file;
};

namespace {

inline std::string_view
toPygmentsLexer(Storage::FileLanguage _lang)
{
    // Convert Doxygen language to Pygments lexer
    // FMI: https://pygments.org/docs/lexers

    static_assert(magic_enum::enum_count<Storage::FileLanguage>() == 20);
    switch (_lang)
    {
        case FileLanguage::C:
            return "c";

        case FileLanguage::Cpp:
            return "cpp";

        case FileLanguage::CSharp:
            return "csharp";

        case FileLanguage::D:
            return "d";

        case FileLanguage::Fortran:
            return "fortran";

        case FileLanguage::FortranFixed:
            return "fortranfixed";

        case FileLanguage::FortranFree:
            return "fortran";

        case FileLanguage::Idl:
            return "idl";

        case FileLanguage::Java:
            return "java";

        case FileLanguage::Javascript:
            return "js";

        case FileLanguage::Lex:
            return "";

        case FileLanguage::Markdown:
            return "markdown";

        case FileLanguage::ObjectiveC:
            return "objective-c";

        case FileLanguage::Php:
            return "php";

        case FileLanguage::Python:
            return "py";

        case FileLanguage::Slice:
            return "systemd";

        case FileLanguage::Sql:
            return "sql";

        case FileLanguage::Verilog:
            return "verilog";

        case FileLanguage::Vhdl:
            return "vhdl";
    }
}

} // namespace
} // namespace Render
} // namespace Inhale

template<>
struct std::formatter<Inhale::Render::LiteralInclude> final
{
    constexpr auto parse(auto &_ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(Inhale::Render::LiteralInclude _data, std::format_context &_ctx) const
    {
        auto const lang = toPygmentsLexer(_data.getLanguage());

        std::format_to(_ctx.out(), ".. literalinclude:: {}\n");  // TODO: Make a relative path.
        if (!lang.empty())
        {
            std::format_to(_ctx.out(), "   :language: {}\n", lang);
        }
        std::format_to(_ctx.out(), "   :linenos:\n");

        return _ctx.out();
    }
};

#endif // __INHALE_RENDER_IH_RST_DIRECTIVES_LITERALINCLUDE_HPP__
