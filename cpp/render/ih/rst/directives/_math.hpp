#ifndef __INHALE_RENDER_IH_RST_DIRECTIVES_MATH_HPP__
#define __INHALE_RENDER_IH_RST_DIRECTIVES_MATH_HPP__

#include <format>
#include <string_view>

namespace Inhale {
namespace Render {

struct Math
{
    std::string_view content;
};

} // namespace Render
} // namespace Inhale

template<>
struct std::formatter<Inhale::Render::Math> final
{
    constexpr auto parse(auto &_ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(Inhale::Render::Math _data, std::format_context &_ctx) const
    {
        return std::format_to(_ctx.out(), ":math:`{}`", _data.content);
    }
};

#endif // __INHALE_RENDER_IH_RST_DIRECTIVES_MATH_HPP__
