#ifndef __INHALE_RENDER_IH_RST_DIRECTIVES_ORPHAN_HPP__
#define __INHALE_RENDER_IH_RST_DIRECTIVES_ORPHAN_HPP__

#include <iterator>
#include <ranges>

namespace Inhale {
namespace Render {

template<std::output_iterator<char> _OutputItT>
void
orphan(_OutputItT _itOut) noexcept
{
    std::ranges::copy(":orphan:\n", _itOut);
}

} // namespace Render
} // namespace Inhale

#endif // __INHALE_RENDER_IH_RST_DIRECTIVES_ORPHAN_HPP__
