#ifndef __INHALE_RENDER_IH_FILENAME_HPP__
#define __INHALE_RENDER_IH_FILENAME_HPP__

#include "inhale/render_config.hpp"

#include "inhale/storage/compound.hpp"

#include <magic_enum.hpp>

#include <format>
#include <ranges>

namespace Inhale {
namespace Render {

struct FileName
{
    Storage::Compound const &compound;
    Config const &config;
};

[[nodiscard]]
inline FileName
makeFileName(Storage::Compound const &_compound, Config const &_config)
{
    return {_compound, _config};
}

} // namespace Render
} // namespace Inhale

namespace {

[[nodiscard]]
inline constexpr std::size_t
getHashedId(std::string_view _str) noexcept
{
    // FMI: https://github.com/Cyan4973/xxHash/issues/124#issuecomment-364282863

    std::size_t seed{5381};
    for (auto const ch : _str)
    {
        seed += ((seed << 5) + seed) + static_cast<std::size_t>(ch);
    }
    return seed;
}

#if __cpp_lib_format_ranges
[[nodiscard]]
auto
getType(Inhale::Storage::CompoundType _type) noexcept
{
    return std::views::transform(
        magic_enum::enum_name(_type),
        [](unsigned char c) noexcept { return std::tolower(c); }
    );
}
#endif

} // namespace

template<>
struct std::formatter<Inhale::Render::FileName> final
{
    constexpr auto parse(auto &_ctx) const noexcept
    {
        return _ctx.begin();
    }

    auto format(Inhale::Render::FileName const &_data, std::format_context &_ctx) const
    {
        if (_data.config.useShortFileNames)
        {
            auto const id = getHashedId(_data.compound.getId());

#if __cpp_lib_format_ranges
            auto const type = getType(_data.compound.getType());
#else
            std::string type{magic_enum::enum_name(_data.compound.getType())};
            type[0] = static_cast<char>(std::tolower(type[0]));
#endif

            return std::format_to(_ctx.out(), "{}_{:x}", type, id);
        }
        else
        {
            return std::format_to(_ctx.out(), "{}", _data.compound.getId());
        }
    }
};

#endif // __INHALE_RENDER_IH_FILENAME_HPP__
