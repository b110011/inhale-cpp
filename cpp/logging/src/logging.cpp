#include "inhale/logging.hpp"

namespace Inhale {
namespace Logging {

static std::unique_ptr<Logger> s_pLogger;

void setLogger(std::unique_ptr<Logger> _pLogger) noexcept
{
    s_pLogger = std::move(_pLogger);
}

Logger &getLogger()
{
    if (s_pLogger == nullptr)
    {
        throw std::runtime_error{"Logger hasn't been set!"};
    }
    return *s_pLogger;
}

} // namespace Logging
} // namespace Inhale
