#ifndef __INHALE_STDLOGGER_HPP__
#define __INHALE_STDLOGGER_HPP__

#include "inhale/logging.hpp"

#include <iostream>  // ignore

namespace Inhale {
namespace Logging {
namespace {

class StdLogger final
    : public Logger
{
private:

    void doLog(Level _level, std::string _message) final noexcept
    {
        switch (_level)
        {
            case Level::Error:
                std::cerr << _message << std::endl;
                break;

            default:
                std::cout << _message << std::endl;
                break;
        }
    }
};

struct StdLoggerInjector
{
    StdLoggerInjector()
    {
        setLogger(std::make_unique<StdLogger>())
    }
};

const StdLoggerInjector g_injector{};

} // namespace
} // namespace Logging
} // namespace Inhale

#endif // __INHALE_STDLOGGER_HPP__
