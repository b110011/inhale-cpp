#ifndef __INHALE_NULLLOGGER_HPP__
#define __INHALE_NULLLOGGER_HPP__

#include "inhale/logging.hpp"

namespace Inhale {
namespace Logging {
namespace {

class NullLogger final
    : public Logger
{
private:

    void doLog(Level /*_level*/, std::string /*_message*/) final noexcept {}
};

struct NullLoggerInjector
{
    NullLoggerInjector()
    {
        setLogger(std::make_unique<NullLogger>())
    }
};

const NullLoggerInjector g_injector{};

} // namespace
} // namespace Logging
} // namespace Inhale

#endif // __INHALE_NULLLOGGER_HPP__
