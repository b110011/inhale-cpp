#ifndef __INHALE_LOGGING_HPP__
#define __INHALE_LOGGING_HPP__

#include <format>
#include <memory>

namespace Inhale {
namespace Logging {

enum class Level: int
{
    Debug = 0,
    Info,
    Warn,
    Error
};

class Logger
{
public:

    virtual ~Logger() = 0;

    template<typename... _ArgsT>
    void log(Level _level, std::format_string<_ArgsT...> _fmt, _ArgsT&&... _args)
    {
        doLog(_level, std::format(_fmt, std::forward<_ArgsT>(_args)...));
    }

private:

    virtual void doLog(Level _level, std::string _message) = 0;
};

void setLogger(std::unique_ptr<Logger> _pLogger) noexcept;

Logger &getLogger();

} // namespace Logging
} // namespace Inhale

#define _INHALE_LOG(LEVEL, FMT, ...) \
    ::Inhale::Logging::getLogger().log(::Inhale::Logging::Level::LEVEL, FMT, __VA_ARGS__);

#define INHALE_DEBUG(_fmt, ...) _INHALE_LOG(Debug, _fmt, __VA_ARGS__)
#define INHALE_INFO(_fmt, ...) _INHALE_LOG(Info, _fmt, __VA_ARGS__)
#define INHALE_WARN(_fmt, ...) _INHALE_LOG(Warn, _fmt, __VA_ARGS__)
#define INHALE_ERROR(_fmt, ...) _INHALE_LOG(Error, _fmt, __VA_ARGS__)

#endif // __INHALE_LOGGING_HPP__
