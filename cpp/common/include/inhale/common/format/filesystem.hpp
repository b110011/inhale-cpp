#ifndef __INHALE_COMMON_FORMAT_FILESYSTEM_HPP__
#define __INHALE_COMMON_FORMAT_FILESYSTEM_HPP__

#include <filesystem>
#include <format>

template <>
struct std::formatter<std::filesystem::path>
    : std::formatter<std::string_view>
{
    auto format(std::filesystem::path const &_path, std::format_context &_ctx) const
    {
        return std::formatter<std::string_view>::format(_path.string(), _ctx);
    }
};

#endif // __INHALE_COMMON_FORMAT_FILESYSTEM_HPP__
