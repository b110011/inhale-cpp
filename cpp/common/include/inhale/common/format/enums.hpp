#ifndef __INHALE_COMMON_FORMAT_ENUMS_HPP__
#define __INHALE_COMMON_FORMAT_ENUMS_HPP__

#include <magic_enum.hpp>

#include <format>
#include <type_traits>

template <typename _EnumT>
    requires std::is_enum_v<_EnumT>
struct std::formatter<_EnumT>
    : std::formatter<std::string_view>
{
    auto format(_EnumT _value, std::format_context &_ctx) const
    {
        return std::formatter<std::string_view>::format(
            magic_enum::enum_name(_value),
            _ctx
        );
    }
};

#endif // __INHALE_COMMON_FORMAT_ENUMS_HPP__
