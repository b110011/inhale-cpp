#ifndef __INHALE_COMMON_FORMAT_HPP__
#define __INHALE_COMMON_FORMAT_HPP__

#include "inhale/common/format/enums.hpp"
#include "inhale/common/format/filesystem.hpp"

#endif // __INHALE_COMMON_FORMAT_HPP__
