from conan import ConanFile


class Inhale(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    default_options = {"gtest/*:hide_symbols": True}
    generators = "CMakeDeps", "CMakeToolchain"

    def requirements(self):
        self.requires("cmopts/latest@b110011/bproto")
        self.requires("magic_enum/0.9.3")
        self.requires("pybind11/[~=2]@")
        self.requires("pugixml/[~=1]@")

    def build_requirements(self):
        self.test_requires("catch2/[~=3]@")
        self.test_requires("gtest/[~=1]@")
