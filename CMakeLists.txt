cmake_minimum_required(VERSION 3.25 FATAL_ERROR)

project(inhale
  LANGUAGES
    CXX
)

#-----------------------------------------------------------------------------#
# Setup                                                                       #
#-----------------------------------------------------------------------------#

set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

#-----------------------------------------------------------------------------#
# Dependencies                                                                #
#-----------------------------------------------------------------------------#

# Generic packages

# NOTE(b110011): This 'find_package' must be placed in top-level CMake script.
#   This way 'enable_testing' and other similar functions can generate data in
#   a correct way.
find_package(cmopts CONFIG REQUIRED)

find_package(magic_enum CONFIG REQUIRED)
find_package(pugixml CONFIG REQUIRED)

# Python packages

find_package(Python 3.2 COMPONENTS Development Interpreter REQUIRED)
find_package(pybind11 CONFIG REQUIRED)

# Tests packages

if(CMOPTS_ENABLE_TESTS)
  find_package(Catch2 3 CONFIG REQUIRED)
endif()

#-----------------------------------------------------------------------------#
# Subdirectories                                                              #
#-----------------------------------------------------------------------------#

add_subdirectory(cpp)
