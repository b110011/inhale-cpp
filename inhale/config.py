from typing import TYPE_CHECKING, Any, TypeAlias, Union

if TYPE_CHECKING:
    from sphinx.application import Sphinx

DocLeafProjects: TypeAlias = dict[str, dict[str, Any]]
DoxygenConfigOptions: TypeAlias = dict[str, str]
ProjectDeps: TypeAlias = dict[str, list[str]]
ProjectInfo: TypeAlias = dict[str, dict[str, Any]]


class ConfigAdaptor:
    def __init__(self, app: "Sphinx", dep: str) -> None:
        self._app = app
        self._dep = dep

    def _get(self, name: str) -> Any:
        return self._app.config[name]

    @property
    def dependencies(self) -> ProjectDeps:
        deps: ProjectDeps = {}

        if self._dep == "docleaf":
            docleaf_deps: DocLeafProjects = self._get("docleaf_projects")
            deps = {k: v.get("deps") for k, v in docleaf_deps.items()}

        if not all([v is not None for v in deps.values()]):
            deps = self._get("inhale_project_deps")

        for project in self.raw_projects.keys():
            if project not in deps:
                deps[project] = []

        return deps

    @property
    def doxygen_config_options(self) -> DoxygenConfigOptions:
        if self._cached_doxygen_config_options:
            return self._cached_doxygen_config_options

        if self._dep == "docleaf":
            opts = self._get("inhale_doxygen_config_options")
        else:
            opts = self._get("breathe_doxygen_config_options")

        if not opts:
            opts = self._get("inhale_doxygen_config_options")

        self._cached_doxygen_config_options = opts
        return opts

    @property
    def projects(self) -> ProjectInfo:
        if self._dep == "docleaf":
            return self._get("docleaf_projects")

        if hasattr(self, "_cached_projects"):
            return self._cached_projects

        projects: dict[str, str] = self._get("breathe_projects")
        projects_source: dict[str, Union[tuple, str]] = self._get("breathe_projects_source")

        self._cached_projects = {}
        for key, value in projects.items():
            root = projects_source[key]
            if isinstance(root, tuple):
                root = root[0]

            self._cached_projects[key] = {"root": root, "xml": value}

        return self._cached_projects

    @property
    def raw_projects(self) -> dict[str, Any]:
        return self._get(f"{self._dep}_projects")

    @raw_projects.setter
    def raw_projects(self, value: dict[str, Any]) -> None:
        self._app.config[f"{self._dep}_projects"] = value



