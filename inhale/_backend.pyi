from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from sphinx.util.logging import SphinxLoggerAdapter

# Logging


def set_logger(logger: "SphinxLoggerAdapter") -> None: ...


def reset_logger() -> None: ...


# Graph


class Context:
    def __init__(self) -> None: ...

    def set_current_project(self, project_name: str) -> None: ...


# Doxygen


class ParserConfig:
    def __init__(self, src_dir: str, xml_dir: str) -> None: ...


def parse_doxygen_output(ctx: Context, config: ParserConfig) -> None: ...


# reST


class RenderConfig:
    def __init__(self, xml_dir: str, rst_dir: str) -> None: ...


def render_rst_files(ctx: Context, config: RenderConfig) -> None: ...
