from sphinx.util import logging

__all__ = ["logger"]

logger = logging.getLogger("inhale")
