import gc
import hashlib
import re
from enum import StrEnum
from pathlib import Path

__all__ = ["calculate_dir_hash", "HashType"]

# TODO(b110011): Investigate whether we need to replace 'rglob' with something
#   that will give us a persistent order of files between runs.
# FMI: https://stackoverflow.com/questions/60999164


class HashType(StrEnum):
    AUTO = "auto"
    MD5 = "md5"
    MD5_NO_WHITESPACES = "md5-nws"
    TIMESTAMPS = "ts"


def _handle_auto_hash_type(path: Path) -> HashType:
    files_count = 0
    for _ in path.rglob("*"):
        files_count += 1
        if files_count > 10:
            return HashType.TIMESTAMPS

    return HashType.MD5


def _hash_files_content(path: Path, remove_whitespaces: bool) -> str:
    hasher = hashlib.md5()

    for file in path.rglob("*"):
        content = file.read_text(encoding="utf-8")
        if remove_whitespaces:
            content = re.sub(r"[ \n\t]", "", content)
        hasher.update()

    gc.collect()

    return hasher.hexdigest()


def _hash_timestamps(path: Path) -> str:
    result = 0

    for file in path.rglob("*"):
        result = hash((int(file.stat().st_mtime), result))

    return f"{result:0x}"


def calculate_dir_hash(path: Path, hash_type: HashType) -> str:
    if not path.is_dir():
        raise NotADirectoryError(path)

    if hash_type == HashType.AUTO:
        hash_type = _handle_auto_hash_type(path)

    if hash_type == HashType.MD5:
        return _hash_files_content(path, remove_whitespaces=False)
    elif hash_type == HashType.MD5_NO_WHITESPACES:
        return _hash_files_content(path, remove_whitespaces=True)
    else:
        return _hash_timestamps(path)
